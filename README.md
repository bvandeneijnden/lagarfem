# LagaRFEM

LagaRFEM is (will be...) a Python package to perform stochastic analyses using Lagamine within the framework of the random finite element method (RFEM). It operates as an intermediate application between the standard executables PREPRO and LAGAMINE and introduces spatially variable material properties 

LagaRFEM is developed on the concept of minimal changes to the core formulations of Lagamine and Prepro, such that only constitutive laws need to be modified. 

The application consists of a GUI, which can be used for all manipulations. The GUI is operating an instance of a python class `RFEM`. The attributes of this class could also be used to operate LagaRFEM directly in a python script, although developments until now have not focussed on the use outside the GUI.


## Past, present and future
The first version of LagaRFEM was created in December 2021, in preparation of the 3rd workshop on the finite element code Lagamine [ref], with the intention of complete itegration within Lapyn [ref]. Some time later, it was built as a stand-alone application to operate between PREPRO and LAGAMINE. 
Eventually, LagaRFEM is to be integrated in Lapyn. Until then, developments can remain in this project.

## Developers
Your name here? Have a look at the TODO list

## Instructions

### Step 0
LagaRFEM can be installed as an independent Python package:

```
pip install https://gitlab.com/bvandeneijnden/lagarfem.git
```

### Step 1
Set up your Lagamine analysis in the usual way, but select one of the implemented constitutive laws that are recognised as constitutive laws for random field analysis. Currently, these are:

| ITYPE | law name | ITYPE RFEM | law name RFEM | ILAW RFEM |
|-------|----------|------------|---------------|-----------| 
|1      | ELA      | 1001       | ELA-RF        | 1001, 1002|
|72     | PLASOL   | 1072       | PLASOL-RF     | 1010      |
|591    | MOHR     | 1591       | MOHR-RF       | 1591      |

These constitutive laws have additional state variables to store material parameter values per integration point. 

LagaRFEM is dependent on a description file with the desription of the RF-compattible laws and the structuring of `PARAM` and `VARIN`. This file is called `RFlaws.json`. Upon introducing (new) constitutive laws to be used in LagaRFEM, they should be consistently added to the JSON file of RF-compattible laws. So far, new RF laws copied from existing ones were given a law number ILAW > 999.

`@RFlaws.json` 
```json
{ "1001": {    "lawName": "ELA-RF",
    "lawDescription": "ELASTIC CONSTITUTIVE LAW FOR SOLID ELEMENTS AT CONSTANT TEMPERATURE",
    "nVariable": 3,
    "iVARIN": [2, 3, 4 ],
    "iPARAM": [2, 3, 4 ],
    "variableName": [
        "E",
        "ANU",
        "RHO"
    ],
    "variableDescription": [
        "YOUNG'S MODULUS",
        "POISSON'S RATIO",
        "SPECIFIC MASS" 
    ]
    },
   "2000": {...
```


### Step 2
* Launch LagaRFEM, preferably in an interactive Python environment:

    ```python
    # import from your package installation (or from where-ever your put in in another form)
    from lagarfem.rfem import Rfem

    # instantiate
    R = Rfem()

    # launch the GUI
    R.GUI()
    ```

    ![dialog_tab_1](figures/ss1.png)

    You can then browse to the simulation that will form the basis of your RFEM analysis. Select which file extensions contain the permanent and (current) variable information and `LOAD`. You can then select laws and corresponding variables and 'sigma'-values for quick visualisation by pressing `PLOT`.
* Open the `parameters` tab.
    This tab now containes the available RFEM-enabled constitutive laws with availabloe variables. Select the required spatial statistics for each variable of each law.  

    ![Alt text](figures/ss2.png)

    The correlation model is defined as follows:

    $$\rho_{tot}(\tau)=r_c+r_n\delta_{\tau=0}+(1-r_n-r_c)\rho(\tau)$$
    
    with:

    $$\tau_{ij}=\sqrt{\left(\frac{x_i-x_j}{\theta_1}\right)^2+\left(\frac{y_i-y_j}{\theta_2}\right)^2+\left(\frac{z_i-z_j}{\theta_3}\right)^2}$$

    Available correlation functions $\rho(\tau)$ are currently `Markov`, `Gaussian` and `Matern`. Note that in the Matern correlation function the length scale $\theta$ is not consistent with the definition of scale of fluctuation. [To be updated]  

    Standard normal random field are generated as:

    $$U=\text{chol}(\mathbf{R})\xi$$

    or if needed using eigendecomposition:

    $$ U=\mathbf{V}\sqrt{\Lambda^*}\mathbf{V}^{T}\xi $$

    in which $\mathbf{V}$ are the eigenvectors of the correlation matrix and $\mathbf{\Lambda^*}$ the diagonal matrix containing the eigenvalues, in which all negative eigenvalues are set to 0. 


    The standard-normal random fields $U$ are then transformed point-wise to the respective marginal distribution $f(X)$ with mean $\mu_X$ and total standard deviation:
    
    $$\sigma_{tot}=\sqrt{\sigma^2+(\mu_X*CoV)^2}$$

    Parameter cross-correlation is currently not implemented.

    ***

    Press `EXAMPLE` to get an example of a random field for the current law and variable, visualised in a scatter plot.

    Press `EXPORT` to export all spatial statistics and generated random fields into a JSON file.

    Press `IMPORT` import previously stored spatial statistics and random fields from a JSON file into the program [TO BE IMPLEMENTED].

    ***

*  open the `generate` tab.

    Select the extension you want to be generated, the files you want to have copied and the number of realisations you want to generate. When pressing `generate`, you will get a number of new folders `./real_xxxx/` in which the variable files, including the random field, are stored.  

    ![Alt text](figures/ss3.png)


*  open the `compute` tab.

    Make sure the correct version of Lagamine is selected as the executable, and press `compute`. This will launch Lagamine on the different realisations in folders `\real_xxxx\`. The results of the individual realisations can be found in their respective subfolders. 
    
    ![Alt text](figures/ss4.png)

    Note that the exit message should be suppressed in Lagamine for automatic processing (if not, you'll have to press `OK` after each realisation...), by including the following early in Lagamine code (e.g. LENAB line 140):

    ```FORTRAN
          IRESULT= SETEXITQQ(QWIN$EXITNOPERSIST)
    ```




## Support

## Development

* To edit the user interface, open `Rfem.ui` in PyQt's `designer`. After editing, translate `.Rfem.ui` into `Ui_Rfem.py` as follows:

    ```
    > python -m PyQt5.uic.pyuic -x Rfem.ui -o Ui._Rfem.py
    ```
* ...

## Roadmap
Some points for development:

- importing RF statistics from `*.json`
- addition of RF-compatible constitutive laws to Lagamine
- dealing with variables outside the constitutive laws (e.g. density)
- extension to 3D

## Contributing

## Authors and acknowledgment

## License
see the LICENSE file. 

