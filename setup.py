from setuptools import setup, find_packages

setup(
    name = 'lagarfem',
    version = '0.0',
    description = 'Random finite element modelling in Lagamine',
    author = 'Bram van den Eijnden',
    author_email = 'bramvandeneijnden@gmail.com',
    packages=find_packages(exclude=('docs','example','figures')),
    include_package_data=True,
    package_data={"": ["*.json"]},
    install_requires = [
        'numpy',
        'scipy',
        'PyQt5',
        'gstools',
        'matplotlib'],
    extras_require={
        'dev': [
            'sphinx',
            'sphinx-rtd-theme',
            ]
        },
    keywords = ['lagamine','rfem'],
    classifiers= [
        "License :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3",
        "Operating System :: Microsoft :: Windows"
        ]
)
