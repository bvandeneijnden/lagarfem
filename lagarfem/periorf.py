

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance_matrix


class perioRF(object):
    '''
    Generator of 2D periodic random fields 

    
    Based on:
        Tee, G.J. (2005) Eigenvectors of block circulant and alternating
        circulant matrices. Res. Lett. Inf. Math. Sci. Vol. 8, pp123-142

        Available online at https://iims.massey.ac.nz/research/letters/

    m > n favours fast factorisation
    n > m favuors fast sampling


    Parameters
    ----------
    m,n : int
        Number of

    theta_m,theta_n : float
        Correlation length scale

    model : str, optional
        correlation model to be used. Options:

            * 'Gaussian'  : rho = exp(-pi*tau**2)
            * 'Markov'    : rho = exp(-2*tau)
            * 'linear'    : rho = max(1 - tau; 0)
            * 'cosine-2'  : rho = .5 + .5*cos( tau / pi)   V tau <  pi  
                                  .0                       V tau >= pi 

        with tau = sqrt((Dx/theta_m)**2 + (Dy/theta_n)**2)        


    
    '''

    def CorrelationModel(self,D,model):
        '''
        spatial dorrelation models based on normalised distance

        Parameters
        ----------
        D : float, array-like
            Normalised distance |Delta x / theta|
        model : str
            correlation model to be used. Options:

                * 'Gaussian'  : rho = exp(-pi*tau**2)
                * 'Markov'    : rho = exp(-2*tau)
                * 'linear'    : rho = max(1 - tau; 0)
                * 'cosine-2'  : rho = .5 + .5*cos( tau / pi)   V tau <  pi  
                                      .0                       V tau >= pi 
        Returns
        -------
        R : array-like 
            correlations evaluated for distances D. Dimensions: D.shape
        
        
        '''

        if model == 'Gaussian':
            R = np.exp(-np.pi*D**2.)
        if model == 'Markov':
            R = np.exp(-2*D)
        if model == 'linear':
            R = np.maximum(1.-D,0)
        if model == 'cosine-2':
            R = 0.5 + 0.5*np.cos(np.minimum(D,1.)*np.pi)

        return R

    def __init__(self,m,n,theta_m,theta_n,model = 'Gaussian',periodic_n = True):
        '''
        see above           
        
        '''

        self.model = model
        self.m = m
        self.n = n
        self.theta_m = theta_m
        self.theta_n = theta_n

        Xm = np.linspace(.5/m,1-.5/m,m).reshape([-1,1])
        Xn = np.linspace(.5/n,1-.5/n,n).reshape([-1,1])

        X,Y = np.meshgrid(Xm,Xn)

        X = X.reshape(-1,1)
        Y = Y.reshape(-1,1)

        DXn = distance_matrix(Xn,Xn)

        if periodic_n:
            DXn = (np.minimum(DXn,1.-DXn)/theta_n)**2
        else:
            DXn = ((DXn)/theta_n)**2

        h = int(m/2)
        b = [None]*(h+1)
        for im in range(h+1):
            DY = (im/m)/theta_m

            D = np.sqrt(DXn + DY**2)

            b[im] = self.CorrelationModel(D,model)

        fnu = np.pi*2/m

        self.v = [None]*h
        self.l = [None]*h

        maxl = 0

        for j in range(h):

            #
            # EQUATION 28
            #
            H_j = b[0].copy()
            for jf in range(1,h):
                fmult = 2.*np.cos(jf*j*fnu)
                H_j += b[jf]*fmult
            H_j += b[h]*(-1.)**j
                
            self.l[j],self.v[j] = np.linalg.eigh(H_j)

            maxl = max(maxl,np.max(self.l[j]))

            if min(self.l[j])<0:
                if -min(self.l[j])/maxl > 1.e-5:
                    print('setting eigenvalues to 0 : ',j, -min(self.l[j])/maxl)
                self.l[j][self.l[j]<0] = 0.

            self.v[j] = np.einsum('ij,j->ij',self.v[j],np.sqrt(self.l[j]))

    def sample_Z(self,**kwargs):
        '''
        Samples a random field

        Parameters
        ----------
        nreal : int, optional
            number of random fields to be generated (default = 1)

        U : array-like [n*m,nreal], optional
            array of i.i.d. standard normal random numbers (default = `numpy.random.normal(size = [m*n,nreal])`)

        Returns
        -------

        Z : array-like [nreal,m,n]
            generated random fields
        

        '''

        m = self.m
        n = self.n
        h = int(m/2)

        fnu = np.pi*2/m

        nreal = kwargs.get('nreal',1)
        self.nreal = nreal
        U = kwargs.get('U',np.random.normal(size = [n*m,nreal]))#

        self.Z = np.zeros([m*n,nreal])

        I_fnu = fnu*np.arange(m)

        for j in range(h):

            C = np.linalg.norm(np.cos(j*I_fnu))
            S = np.linalg.norm(np.cos(j*I_fnu))
            if j == 0:
                S = 1. # to prevent 0./0.

            TS = self.v[j] @ U[j*n:(j+1)*n]
            TC = self.v[j] @ U[(j+h)*n:(j+h+1)*n]
            
            for i in range(m):
                c = np.cos(i*j*fnu)
                s = np.sin(i*j*fnu)

                self.Z[i*n:(i+1)*n] += (c*TC/C +  s*TS/S)

        self.Z = self.Z.T

        return self.Z
        
    def show_Z(self,**kwargs):
        '''
        Visualises the random field using pyplot.imshow()

        Parameters
        ----------
        ireal : int, optional
            realisation number ``0<=ireal<nreal`` to be plotted

        extent : floats , optional
            positioning and dimensions of field (left,right,bottom,top) (default = (0.,1.,0.,1.))

        aspect : float, optional
            plotting aspect ratio (default = 1.)

        Returns
        -------
        empty
                        
        '''
        m = self.m
        n = self.n

        extent = kwargs.get('extent',(0.,1.,0.,1.))
        aspect = kwargs.get('aspect',1.)
        ireal = kwargs.get('ireal',0)

        if ireal >= self.nreal:
            print('only %i realisations generated'%(self.nreal))
            ireal = 0. 

        plt.imshow(self.Z[:,ireal].reshape([m,n]).T,
                   extent = extent,aspect = aspect)
        
        plt.title('periodic %ix%i random field %i'%(m,n,ireal))
        plt.colorbar(label = 'standard normal U~N(0,1)$')
        
if __name__ == '__main__':

    '''
    Four examples of random fields with identical scales of 
    fluctuation.
    
    '''
    prf = perioRF(200,200,0.25,0.03,model = 'Markov')
    prf.sample_Z(nreal = 1)
    prf.show_Z()    
    plt.title('Markov correlation finction')

    prf = perioRF(200,200,0.25,0.03,model = 'Gaussian')
    prf.sample_Z(nreal = 1)
    plt.figure()
    prf.show_Z()        
    plt.title('Gaussian correlation function')

    prf = perioRF(200,200,0.25,0.03,model = 'linear')
    prf.sample_Z(nreal = 1)
    plt.figure()
    prf.show_Z()        
    plt.title('linear correlation function')

    prf = perioRF(200,200,0.25,0.03,model = 'cosine-2')
    Z = prf.sample_Z(nreal = 10)
    plt.figure()
    prf.show_Z()
    plt.title('cosine-squared correlation function')

    plt.figure()
    plt.imshow(Z.reshape([200,200]).T,extent = (0.,1.,0.,1.))
    plt.imshow(Z.reshape([200,200]).T,extent = (0.,1.,1.,2.))
    plt.imshow(Z.reshape([200,200]).T,extent = (1.,2.,0.,1.))
    plt.imshow(Z.reshape([200,200]).T,extent = (1.,2.,1.,2.))
    
    plt.plot([0,2,2,0,0],[0,0,2,2,0],'--r')
    plt.plot([1,1],[0,2],'--r')
    plt.plot([0,2],[1,1],'--r')
    
    plt.xlim([-.05,2.05])
    plt.ylim([-.05,2.05])
    plt.xticks([])
    plt.yticks([])

    plt.savefig('example_periodic.png',transparent = True)



    
