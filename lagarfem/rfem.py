"""

LagaLaw class and material law classes


"""
import sys,os
sys.path.append('../')
sys.path.append('../')

from scipy.io import FortranFile
import numpy as np
import matplotlib.pyplot as plt

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject,QDir, pyqtSlot
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QFileDialog,QFileSystemModel,QApplication,QShortcut,QAbstractButton,QMessageBox,QTableWidgetItem
import subprocess

try:
    # works when used as a pip-installed package
    from .Ui_Rfem import Ui_DialogRfem
    from .periorf import perioRF
except:
    # works when used by running the file directly
    from Ui_Rfem import Ui_DialogRfem
    from periorf import perioRF


#from ui_forms.Ui_LoadingFile import Ui_DialogLoadingFile
from copy import deepcopy

import gstools as gs
from gstools.random import MasterRNG

from datetime import datetime

#global Ui


class RFlaw():
    ''' class for random fields per constitutive law
    
    TODO: - marginal model based on scipy.stats
          - 
    
    '''
    def __init__(self,lawnumber,lawname,I):

        import json

        self.lawindex = int(I)
        self.lawname = lawname
        self.lawnumber = int(lawnumber)

        self.nvariable = 0
        self.iVARIN = []
        self.variableName = []
        self.marginalModel = []
        self.spatialModel = []
        self.constantRatio = []
        self.noiseRatio = []
        self.isotropic = []
        self.periodic = []
        self.theta = []
        self.mean = []
        self.sigma = []
        self.CoV = []
        self.nu = []
        self.seed = []  
        self.RF = []

        try:
            F = json.loads(open('RFlaws.json','r').read()).get(str(lawnumber))
        except:
            import pkg_resources
            file_path = pkg_resources.resource_filename('lagarfem', 'RFlaws.json')
            F = json.loads(open(file_path,'r').read()).get(str(lawnumber))

        if F != None:

            self.nvariable = F['nVariable']
            self.RF = [np.array([])]*self.nvariable
            self.seed = [0]*self.nvariable
            self.CoV = [0.]*self.nvariable
            self.sigma = [0.]*self.nvariable
            self.theta = [[1.,1.,1.] for i in range(self.nvariable)]
            
            self.iVARIN = F['iVARIN']
            self.iPARAM = F['iPARAM']
            self.variableName = F['variableName']
            self.variableDescription = F['variableDescription']
            self.marginalModel = ['Normal']*self.nvariable
            self.spatialModel = ['Markov']*self.nvariable
            self.constantRatio = [0.]*self.nvariable
            self.noiseRatio = [0.]*self.nvariable
            self.mean = [None]*self.nvariable
            self.nu = [1.5]*self.nvariable
            self.isotropic = [False]*self.nvariable
            self.periodic = [False]*self.nvariable
        
        self.correlation = np.eye(self.nvariable)

    def export_to_json(self,jfile):
        import json

        print('>> __.export_to_json(',jfile,')')

        RFdict = {'lawindex':self.lawindex,
                  'lawname':self.lawname,
                  'lawnumber':self.lawnumber,
                  'nvariable':self.nvariable,
                  'iVARIN':self.iVARIN,
                  'iPARAM':self.iPARAM,
                  'variableName':self.variableName,
                  'variableDescription':self.variableDescription,
                  'marginalModel':self.marginalModel,
                  'spatialModel':self.spatialModel,
                  'constantRatio':self.constantRatio,
                  'noiseRatio':self.noiseRatio,
                  'isotropic':self.isotropic,
                  'periodic':self.periodic,
                  'theta':self.theta,
                  'mean':self.mean,
                  'sigma':self.sigma,
                  'cov':self.CoV,
                  'nu':self.nu,
                  'correlation':self.correlation.tolist(),
                  'seed':self.seed,
                  'RF':[i.tolist() for i in self.RF]
                  }
        json.dump(RFdict,jfile,indent = 4)
        return

    def import_from_json(self):
        '''
        TO BE IMPLEMENTED
        '''
        pass

class Rfem(Ui_DialogRfem):
    ''' class for LagaLaw interface '''
    def __init__(self,filename = None, path = None, DFS = False, LFS = False, lag = None):
        import json 

        self.DataFileSelected = DFS
        self.LoadFileSelected = LFS
        
        if path == None and filename != None:
            head,tail = os.path.split(filename)
            self.path = head
        else:
            self.path = path
        
        self.filename = filename
        
        self.per = '.f01'
        self.var = '.f02'

        try:
            self.F = json.loads(open('RFlaws.json','r').read())
        except:
            import pkg_resources
            file_path = pkg_resources.resource_filename('lagarfem', 'RFlaws.json')
            self.F = json.loads(open(file_path,'r').read())
        
        self.RFlaws = []
        self.periodic = False
        self.nfield = 1

        self.Ui = Ui_DialogRfem()
    
    def write_files(self, per_new = None, var_new = None, dir = None):


        if per_new == None:
            per_new = self.per
        if var_new == None:
            var_new = self.var
            
        print('>> __.write_files('+per_new+','+var_new+')')

            
        #% WRITE F01
        #if dir == None:
        #    f01 = FortranFile(self.path + '\\' + self.filename + per_new,'w')
        #else:
        #    f01 = FortranFile(dir + '\\' + self.filename + per_new,'w')
        #           
        #f01.write_record(self.per_1)
        #f01.write_record(self.per_2)
        #for line in self.per_n:
        #    f01.write_record(line)
        #if self.per_1['NTANA'] < 0:
        #    f01.write(self.per_04)
        #f01.write_record(self.per_4)
        #
        #f01.close()


        if dir == None:
            f02 = FortranFile(self.path + '\\' + self.filename + var_new,'w')
        else:
            f02 = FortranFile(dir + '\\' + self.filename + var_new,'w')

        f02.write_record(self.var_1)
        f02.write_record(self.var_2)
        for L,S,V in zip(self.LSTAT,self.SIGMA,self.VARIN):
            f02.write_record(L,S,V)

        f02.close()        

    def read_files(self,per = None,var = None):

        if per != None:
            self.per = per
        if var != None:
            self.var = var
        
        if per != None:
            self.per = per
        if var != None:
            self.var = var

        print('>> __.read_files('+self.per,',',self.var,')')
        
        #% READ F01

        #% --- line 1 ---
        full_filename = self.path + '/' + self.filename + self.per
        f01 = FortranFile(full_filename,'r')
        self.dtype_per_1 = np.dtype([
            ('TITLE','a72'),('NUMNP','i'),  ('NSPAC','i'),  ('NDOFN','i'),  
            ('NEQUA','i'),  ('NDISP','i'),  ('NLAW','i'),   ('MPARA','i'),  
            ('MPARI','i'),  ('NELEM','i'),  ('NTANA','i'),  ('IANA','i'),
            ('NCONE','i'),  ('NSEGT','i'),  ('NMASC','i'),  ('NDAMC','i'),  
            ('IMDIS','i'),  ('ICRIT','i'),  ('IDMAT','i'),  ('IRENU','i'),  
            ('NLAWM','i'),  ('MPAME','i'),  ('NTCOU','i'),  ('INDVG','i'),
            ('INDMG','i'),  ('NFOUN','i'),  ('NVERS','i'),  ('IERRO','i'),  
            ('ICGEO','i'),  ('IRICE','i'),  ('IPIFO','i'),  ('IAXLO','i'),  
            ('ITREE','i'),  ('GRAVIX','f8'),('GRAVIY','f8'),('GRAVIZ','f8'),
            ('MAXDUP','i'), ('META','i'),   ('INCFO','i'),  ('MAXELC','i'), 
            ('NBIFOR','i'), ('NBRFOR','i')
            ])


        self.per_1 = f01.read_record(self.dtype_per_1)

        NDOFN = self.per_1['NDOFN'][0]
        NUMNP = self.per_1['NUMNP'][0]
        NEQUA = self.per_1['NEQUA'][0]
        MPARA = self.per_1['MPARA'][0]
        MPARI = self.per_1['MPARI'][0]
        MPAME = self.per_1['MPAME'][0]
        NLAWM = self.per_1['NLAWM'][0]
        NSEGT = self.per_1['NSEGT'][0]
        NFOUN = self.per_1['NFOUN'][0]
        NSPAC = self.per_1['NSPAC'][0]
        MAXELC = self.per_1['MAXELC'][0]
        NELEM = self.per_1['NELEM'][0]
        NBIFOR = self.per_1['NBIFOR'][0]
        NBRFOR = self.per_1['NBRFOR'][0]
        IANA = self.per_1['IANA'][0]

        NLAW = self.per_1['NLAW'][0]

        NDISP1= max(1,self.per_1['NDISP'][0])
        NMASC1= max(1,self.per_1['NMASC'][0])
        NDAMC1= max(1,self.per_1['NDAMC'][0])
        
        
        NCOL=1
        if self.per_1['IMDIS'] == 2:
            NCOL=2
        if self.per_1['IMDIS'] == 3:
            NCOL=3
        if self.per_1['NTANA'] <= 0:
            NCOL=3
        if self.per_1['IMDIS'] >= 10:
            NCOL+=1




        #% --- line 2 ---

        self.dtype_per_2 = np.dtype([
            ('ID0','i', (NDOFN,NUMNP)),    ('DISPI','f8',(NDISP1,)),
            ('IDIMAS','i', (2*NMASC1,)),   ('IDIDAM','i',(2*NDAMC1,)),
            ('FOMADA0','f8',(NEQUA,NCOL)), ('PARAM','f8',(NLAW,MPARA)),
            ('LTYPE','i',(NLAW,MPARI)),    ('PAMET','f8',(MPAME,NLAWM)),
            ('IFONDA','i',(18*NSEGT,)),    ('NOPILO0','i',(NUMNP,)),
            ('EXCENT0','f8',(3*NUMNP,)),   ('XYZLI','f8',(2*NSPAC*NFOUN,)),
            ('IFIFO','i',(NSPAC*NFOUN,)),  ('NROPI','i',(NFOUN,)),
            ('MCONEL0','i',(MAXELC,NELEM)),('IFORM','i',(NBIFOR,)),
            ('RFORM','f8',(NBRFOR,))
            ])

        self.per_2 = f01.read_record(self.dtype_per_2)
             
        self.LTYPE = self.per_2['LTYPE'][0].T
        self.PARAM = self.per_2['PARAM'][0]
            
        #% --- line n ---

        MXIGA = 0
        self.LNIP = [] #np.zeros(NELEM)
        self.LUNIT = [] #np.zeros(NELEM)
        self.KTYPE = []
        self.NNODE = []
        self.NDOFE = []

        self.NODES = []
        self.per_n = []
        
        
        
        for ielem in range(NELEM):
            
            l = f01.read_ints()
            KTYPE = l[0]
            NNODE = l[1]
            NDOFE = l[2]
            NELPA0 = l[3]
            NEXTR0 = l[4]

            self.NODES.append(l[5:5+NNODE])
            LM = l[5+NNODE:5+NNODE+NDOFE]
            LELEM = l[5+NNODE+NDOFE:5+NNODE+NDOFE+NELPA0]

            self.KTYPE.append(KTYPE)
            self.NNODE.append(NNODE)
            self.NDOFE.append(NDOFE)
            if KTYPE == 99:
                l = f01.read_ints()     
            elif KTYPE == 225:
                #
                # TODO :: currently only for MWAT3, may have to be extendded for all 3D elements?
                #
                NIP = LELEM[1]*LELEM[2]*LELEM[3]      
                self.LNIP.append([NIP])
                
            else:
                NIP = LELEM[1::3]      
                self.LNIP.append(NIP)
            
            if KTYPE == 225:
                IUNIT = LELEM[4:6]
                self.LUNIT.append(IUNIT)

                if NEXTR0 == 1:
                    #
                    # TODO !!!
                    # 
                    tmp = f01.read_record('1i4')
                    MSIGA = tmp[0]                    
                    MXIGA = max(MXIGA,MSIGA)

            elif len(LELEM) > 2:
                IUNIT = LELEM[2::3]
                self.LUNIT.append(IUNIT)

        
                if NEXTR0 == 1:
                    #
                    # TODO !!!
                    # 
                    tmp = f01.read_record('1i4')
                    MSIGA = tmp[0]                    
                    MXIGA = max(MXIGA,MSIGA)
            else:
                #print(ielem,LELEM)
                pass

            self.per_n.append(l)
        self.MXIGA = MXIGA

        #% --- line 4 ---
            
        if self.per_1['NTANA'] < 0:
            self.dtype_per_4 = np.dtype([('NTPER','i')])
            self.per_04 = f01.read_record(self.dtype_per_4)
    

        self.per_4 = f01.read_ints()
        
        #self.MBAND = line_4[0]
        #self.NHICO = line_4[1:NEQUA+1]
        #self.NELTP = line_4[NEQUA+1]
        #self.NCOLG = line_4[NEQUA+2]
        #self.LIGRO = line_4[NEQUA+3:self.MBAND+3+self.NELTP*self.NCOLG]
        
        #self.dtype_per_4 = np.dtype([
        #    ('MBAND','i'),    
        #    ('NHICO','i', (self.MBAND,)),
        #    ('NELTP','i'), 
        #    ('NCOLG','i',),    
        #    ('LIGRO','i',(self.NCOLG*self.NELTP))
        #    ])
        
        

        #% READ F02 / F03

        full_filename = self.path + '/' + self.filename + self.var
        f02 = FortranFile(full_filename,'r')

        #% --- line 1 ---

        self.dtype_var_1 = np.dtype([
            ('ALACUM','f8'),
            ('ALAMF','f8'),
            ('TIME','f8'),
            ('TITLE','a72'),
            ('CPUMT','f8',7),
            ('ITOIT','i'),
            ('ISTEP','i')            
            ])
        
        self.var_1 = f02.read_record(self.dtype_var_1)
        
        
        #% --- line 2 ---
        
        # -------------------------------------------------------
        l = f02.read_record('i')       
        NUMNP = l[0]
        NSPAC = l[1]
        NCONE = l[2]
        NNN = l[3 + NUMNP*NSPAC*NCONE*2]
        
        f02.close()
        
        full_filename = self.path + '/' + self.filename + self.var
        f02 = FortranFile(full_filename,'r')
        f02.read_record('i')

        # -------------------------------------------------------

        self.dtype_var_2 = np.dtype([
            ('NUMNP','i'),
            ('NSPAC','i'),
            ('NCONE','i'),
            ('CONEC','f8',(NCONE,NSPAC,NUMNP)),
            ('NNN','i'),
            ('DUM','f8',(NNN)),
            ('WINTT','f8'),
            ('WDYNT','f8')
            ])
        
        self.var_2 = f02.read_record(self.dtype_var_2)
        
        self.CONEC = self.var_2['CONEC'][0]
        
        #% --- line n ---
        
        self.LSTAT = []
        
        MVARI = np.max(self.LTYPE[1,:])
        
        self.SIGMA = [None]*NELEM
        self.VARIN = [None]*NELEM
        
        
        for ielem in range(NELEM):


            if self.KTYPE[ielem] == 99:
                #
                # BINDS ELEMENTS
                #
                dtype_i = np.dtype([('NSTAT','i'),
                               ('SIGMA','f8'),
                               ('VARIN','f8',3)])

            elif self.KTYPE[ielem] == 15:
                #
                # LICHA ELEMENTS
                #
                NIP = self.LNIP[ielem]

                # because for some reason LICHA elements are not included in LUNIT,
                # he following works only for one type of LICHA elements per simulation, 
                # i.e. no combination of 2-IP and 3-IP LICHA elements...
                #  
                # Finds the place pf the constitutive law (95) in LTYPE
                iunit = int(np.argwhere(self.LTYPE[0]==95))

                MVARI = self.LTYPE[1,iunit]
                NSIG =  self.LTYPE[2,iunit]

                dtype_i = np.dtype([('NSTAT','i'),
                               ('SIGMA','f8',(NIP[0],NSIG)),
                               ('VARIN','f8',(NIP[0],MVARI))])

            else:
                #
                # ALL OTHER ELEMENTS
                #
                IUNIT = self.LUNIT[ielem]
                NIP = self.LNIP[ielem]
                
                MVARI = [self.LTYPE[1,iunit-1] for iunit in IUNIT]
                NSIG =  [self.LTYPE[2,iunit-1] for iunit in IUNIT]
                #
                # TO CHECK
                # currently only for elements with equal integration points for all laws
                # probably needs to be extended for HM coupling and certainly for 
                # second gradient elements...
                #
                # should probably be something like: NSIG_TOT = dot_product(NIP,NSIG)
                # 
                dtype_i = np.dtype([('NSTAT','i'),
                               ('SIGMA','f8',(NIP[0],np.sum(NSIG))),
                               ('VARIN','f8',(NIP[0],np.sum(MVARI)))])

            l = f02.read_record(dtype_i)
            self.LSTAT.append(l['NSTAT'])
            self.SIGMA[ielem] = l['SIGMA'][0]
            self.VARIN[ielem] = l['VARIN'][0]
            

        f01.close()
        f02.close()

    def plot_mesh(self):
        
        print('>> __.plot_mesh()') 
        x = [np.array([])]*self.per_1['NLAW'][0]
        y = [np.array([])]*self.per_1['NLAW'][0]
        z = [np.array([])]*self.per_1['NLAW'][0]

        for ielem in range(len(self.LUNIT)):
            n = self.NODES[ielem]
            for iu in self.LUNIT[ielem]: 
                iunit = iu-1
                ilaw = self.per_2['LTYPE'][0][iunit,0]
                x[iunit] = np.hstack([x[iunit],self.CONEC[0,0,n-1]])
                x[iunit] = np.hstack([x[iunit],self.CONEC[0,0,n[0]-1]])
                x[iunit] = np.hstack([x[iunit],np.nan])
                y[iunit] = np.hstack([y[iunit],self.CONEC[0,1,n-1]])
                y[iunit] = np.hstack([y[iunit],self.CONEC[0,1,n[0]-1]])
                y[iunit] = np.hstack([y[iunit],np.nan])
        iunit = -1
        for xi,yi in zip(x,y):            
            iunit += 1
            ilaw = self.per_2['LTYPE'][0][iunit,0]
            
            if ilaw < 10000:
                plt.plot(np.array(xi),np.array(yi),linewidth = .5)
            else:
                plt.plot(np.array(xi),np.array(yi),label = 'law %i'%(ilaw))
        plt.legend()      
        plt.axis('equal')
        plt.show()
        
    def make_example(self):
        
        print('>> __.make_example()')
        # self.generateRF_GS()
        self.plot_ip_values_GS(SEED=False)

    def plot_ip_values(self):
        print('>> __.plot_ip_values()')
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        
        xip,yip,zip,einl = self.extract_IPcoord()
        #
        # TODO :: copy variable from correct law in case of multiple 
        # laws per element
        #

        kvar = 0

        if ilaw > 0:
            kvar += self.LTYPE[1][ilaw-1]

        zip = []
        IL = self.Ui.colaws_with_RF[ilaw]-1
        for ielem in einl[IL]:
            nip = self.LNIP[ielem][0]
            for ip in range(nip):
                var = self.VARIN[ielem][ip,kvar+self.RFlaws[ilaw].iVARIN[ivar]-1]
                zip.append(var)
        
        plt.scatter(xip[IL],yip[IL],c = zip)
        plt.colorbar()
        plt.show()

    def plot_ip_values_GS(self, SEED=True):
        print('>> __.plot_ip_values()')
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        print('ilaw: ', ilaw)
        print('ivar: ', ivar)
        srfs = self.generateRF_GS(SEED=SEED)
        print(srfs[ilaw])
        srf = srfs[ilaw][ivar]

        srf.plot()
        plt.show()


    def plot_ip_values_2(self):
        ilaw = self.Ui.comboBoxColawPlot.currentIndex()
        ivar = self.Ui.comboBoxVariablePlot.currentIndex()
        #
        # TODO :: copy variable from correct law in case of multiple 
        # laws per element
        #
        xip,yip,zip,einl = self.extract_IPcoord()
        zip = []
        for ielem in einl[ilaw]:
            nip = self.LNIP[ielem][0]
            for ip in range(nip):
                var = self.VARIN[ielem][ip,ivar]
                zip.append(var)
        plt.figure()
        plt.scatter(xip[ilaw],yip[ilaw],c = zip)
        plt.colorbar()
        fig = plt.gcf()
        plt.show()
        fig.canvas.flush_events()

    def plot_ip_sigma(self):

        print('>> __.plot_ip_sigma()')
        ilaw = self.Ui.comboBoxColawPlot.currentIndex()
        isig = self.Ui.comboBoxSigmaPlot.currentIndex()
        
        xip,yip,zip,einl = self.extract_IPcoord()
        zip = []
        for ielem in einl[ilaw]:
            nip = self.LNIP[ielem][0]
            for ip in range(nip):
                var = self.SIGMA[ielem][ip,isig]
                zip.append(var)
        
        plt.scatter(xip[ilaw],yip[ilaw],c = zip)
        fig = plt.gcf()
        plt.show()
        if self.Ui.checkBoxAddColorbar.isChecked():
            plt.colorbar()

        fig.canvas.flush_events()

    def _shape_functions(self,):
        
        IANA = self.per_1['IANA'][0]

        if IANA != 4:
            xi = np.sqrt(1./3.)*np.array([-1.,1.,1.,-1.])
            et = np.sqrt(1./3.)*np.array([-1.,-1.,1.,1.])
            
            N = np.array([(1.-xi)*(et-1)*(xi+et+1)/4.,
                          (1.-et)*(1-xi**2)/2.,
                          (1.+xi)*(et-1)*(-xi+et+1)/4.,
                          (1.+xi)*(1-et**2)/2.,
                          (1.+xi)*(et+1)*(xi+et-1)/4.,
                          (1.+et)*(1-xi**2)/2.,
                          (xi-1.)*(et+1)*(xi-et+1)/4.,
                          (1.-xi)*(1-et**2)/2.])

        elif IANA == 4:            
            # xi, eta and zeta are not checked yet
            xi = np.sqrt(1./3.)*np.array([-1.,1.,1.,-1.,-1.,1.,1.,-1.])
            eta = np.sqrt(1./3.)*np.array([-1.,-1.,1.,1.,-1.,-1.,1.,1.])
            zeta = np.sqrt(1./3.)*np.array([-1.,-1.,-1.,-1.,1.,1.,1.,1.])

            N = np.array([
                        (1. - xi) * (1 - eta) * (1 - zeta) * (-2 - xi - eta - zeta) / 8.,
                        (1. + xi) * (1 - eta) * (1 - zeta) * (-2 + xi - eta - zeta) / 8.,
                        (1. + xi) * (1 + eta) * (1 - zeta) * (-2 + xi + eta - zeta) / 8.,
                        (1. - xi) * (1 + eta) * (1 - zeta) * (-2  -xi + eta - zeta) / 8.,
                        (1. - xi) * (1 - eta) * (1 + zeta) * (-2 - xi - eta + zeta) / 8.,
                        (1. + xi) * (1 - eta) * (1 + zeta) * (-2 + xi - eta + zeta) / 8.,
                        (1. + xi) * (1 + eta) * (1 + zeta) * (-2 + xi + eta + zeta) / 8.,
                        (1. - xi) * (1 + eta) * (1 + zeta) * (-2 - xi + eta + zeta) / 8.,
                        (1. - xi**2) * (1 - eta) * (1 - zeta) / 4.,
                        (1. + xi) * (1 - eta**2) * (1 - zeta) / 4.,
                        (1. - xi**2) * (1 + eta) * (1 - zeta) / 4.,
                        (1. - xi) * (1 - eta**2) * (1 - zeta) / 4.,
                        (1. - xi**2) * (1 - eta) * (1 + zeta) / 4.,
                        (1. + xi) * (1 - eta**2) * (1 + zeta) / 4.,
                        (1. - xi**2) * (1 + eta) * (1 + zeta) / 4.,
                        (1. - xi) * (1 - eta**2) * (1 + zeta) / 4.,
                        (1. - xi) * (1 - eta) * (1 - zeta**2) / 4.,
                        (1. + xi) * (1 - eta) * (1 - zeta**2) / 4.,
                        (1. + xi) * (1 + eta) * (1 - zeta**2) / 4.,
                        (1. - xi) * (1 + eta) * (1 - zeta**2) / 4.])
        
        
        else:
            N = 1
        return N
            
    def extract_IPcoord(self):

        IANA = self.per_1['IANA'][0]
    
        print('>> __.extract_IPcoord()')
        x = [np.array([])]*self.per_1['NLAW'][0]
        y = [np.array([])]*self.per_1['NLAW'][0]
        z = [np.array([])]*self.per_1['NLAW'][0]

        lElementsPerLaw = [np.array([],dtype = 'int')]*self.per_1['NLAW'][0]
        

        for ielem in range(len(self.LUNIT)):

            N = self._shape_functions()

            n = self.NODES[ielem]
            ilaws = self.LUNIT[ielem]-1
            
            xe = self.CONEC[0,0,n-1]
            ye = self.CONEC[0,1,n-1]
            ze = self.CONEC[0,2,n-1]                # Takes PW if 2D right now, needs to be fixed
            
            xip = N.T @ xe
            yip = N.T @ ye
            zip = N.T @ ze

            for ilaw in ilaws:
                x[ilaw] = np.hstack([x[ilaw],xip])
                y[ilaw] = np.hstack([y[ilaw],yip])
                z[ilaw] = np.hstack([z[ilaw],zip])
                lElementsPerLaw[ilaw] = np.hstack([lElementsPerLaw[ilaw],ielem])
        
        # If statement to set z equal to zero if 2D=True?
        # The, how to check whether 2D? From .lag file?

        if IANA != 4:
            z = [np.zeros_like(xi) for xi in x]
    
        return x,y,z,lElementsPerLaw
        
        
    def GUI(self):
        '''
        Run the GUI

        '''

        #self.read_files()
        #self.plot_mesh()

        if QtWidgets.QApplication.instance() is None:
            app = QtWidgets.QApplication(sys.argv)
        else:
            app = QtWidgets.QApplication.instance()

        
        self.DialogRfem = QtWidgets.QDialog()
        
        self.Ui = Ui_DialogRfem()
        self.Ui.setupUi(self.DialogRfem) # initiate   
        self.setupUi()

        if self.filename != None:
            full_filename = self.path+'/'+self.filename+'.lag'
            self.Ui.lineEditLagPath.setText(full_filename)

        self.DialogRfem.show()
        self.DialogRfem.exec_()

    def loadFiles(self):

        self.read_files()

        if self.Ui.checkBoxPlotMesh.isChecked():
            self.plot_mesh()


        self.Ui.comboBoxColaw.clear()
        self.Ui.comboBoxMarginal.clear()
        for marginal in ['Normal','LogNormal']:
            self.Ui.comboBoxMarginal.addItem(marginal)


        unique_list = {x for l in self.LUNIT for x in l}
        self.Ui.colaws_with_RF = []

        self.RFlaws = []


        for I in unique_list:
            lawnumber = int(self.LTYPE[0,I-1])
            slawnumber = str(lawnumber)
            F = self.F.get(slawnumber)

            if F:
                slawname = F['lawName']
                self.Ui.comboBoxColaw.addItem(str(I)+' - '+ slawnumber +' - ' + slawname)
                self.Ui.colaws_with_RF.append(I)

                self.RFlaws.append(RFlaw(lawnumber,slawname,I))

                for i in range(self.RFlaws[-1].nvariable):
                    ipar = self.RFlaws[-1].iPARAM[i]-1
                    self.RFlaws[-1].mean[i] = self.PARAM[I-1,ipar]

        if len(self.RFlaws) > 0:
            self.colawChanged()
            self.Ui.tabParameters.setEnabled(True)
            self.Ui.tabGenerate.setEnabled(True)
            self.Ui.tabCompute.setEnabled(True)
        
        self.loadVariablesPlot()

    def loadVariablesPlot(self):

        self.Ui.comboBoxColawPlot.setEnabled(True)
        self.Ui.comboBoxVariablePlot.setEnabled(True)

        nlaw = self.per_1['NLAW'][0]

        self.Ui.comboBoxColawPlot.clear()
        for i in range(1,nlaw+1):
            self.Ui.comboBoxColawPlot.addItem(str(i))


        self.Ui.comboBoxColawPlot.clear()
        for i in range(1,nlaw+1):
            self.Ui.comboBoxColawPlot.addItem(str(i))




        pass


    def colawPlotChanged(self):

        ilaw = self.Ui.comboBoxColawPlot.currentIndex()
        nvari = self.LTYPE[1,ilaw]
        self.Ui.comboBoxVariablePlot.clear()
        for i in range(1,nvari+1):
            self.Ui.comboBoxVariablePlot.addItem(str(i))
            
        nsig = self.LTYPE[2,ilaw]
        self.Ui.comboBoxSigmaPlot.clear()
        for i in range(1,nsig+1):
            self.Ui.comboBoxSigmaPlot.addItem(str(i))


    def colawChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        self.Ui.comboBoxVariable.clear()
        if len(self.RFlaws)>0:
            for name in self.RFlaws[ilaw].variableName:
                self.Ui.comboBoxVariable.addItem(name)

            self.loadParameters()


    def loadParameters(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()

        law = self.RFlaws[ilaw]

        self.Ui.comboBoxMarginal.setCurrentText(law.marginalModel[ivar])
        self.Ui.lineEditMean.setText(str(law.mean[ivar]))
        self.Ui.lineEditSigma.setText(str(law.sigma[ivar]))
        self.Ui.lineEditCoV.setText(str(law.CoV[ivar]))

        self.Ui.comboBoxSpatial.setCurrentText(str(law.spatialModel[ivar]))
        self.Ui.lineEditNu.setText(str(law.nu[ivar]))
        self.Ui.lineEditNoiseRatio.setText(str(law.noiseRatio[ivar]))
        self.Ui.lineEditConstantRatio.setText(str(law.constantRatio[ivar]))

        self.Ui.checkBoxIsotropic.setCheckState(law.isotropic[ivar])
        self.Ui.checkBoxPeriodic.setCheckState(law.periodic[ivar])

        self.Ui.lineEditTheta2.setEnabled(not law.isotropic[ivar])
        self.Ui.lineEditTheta3.setEnabled(not law.isotropic[ivar])
        self.Ui.lineEditNu.setEnabled(law.spatialModel[ivar]=='Matern')

        self.Ui.lineEditTheta1.setText(str(law.theta[ivar][0]))
        self.Ui.lineEditTheta2.setText(str(law.theta[ivar][1]))
        self.Ui.lineEditTheta3.setText(str(law.theta[ivar][2]))
        print('theta loaded', str(law.theta[ivar][0]))
        labels =  self.RFlaws[ilaw].variableName
        nvar = self.RFlaws[ilaw].nvariable

        self.Ui.tableWidgetCorrelation.setColumnCount(nvar)
        self.Ui.tableWidgetCorrelation.setRowCount(nvar)
        self.Ui.tableWidgetCorrelation.setHorizontalHeaderLabels(labels)
        self.Ui.tableWidgetCorrelation.setVerticalHeaderLabels(labels)

        for i,name in enumerate(labels):
            for j,namej in enumerate(labels):
                c = str(self.RFlaws[ilaw].correlation[i,j])
                self.Ui.tableWidgetCorrelation.setItem(i,j,QTableWidgetItem(c))

    def correlChanged(self,k,l,i,j):
        ilaw = self.Ui.comboBoxColaw.currentIndex()

        try:
            rho = round(float(self.Ui.tableWidgetCorrelation.item(i,j).data(0)),3)
            if np.abs(rho) > 1.:
                rho = np.sign(rho)
        except:
            print('incorrect rho_ij:',self.Ui.tableWidgetCorrelation.item(i,j).data(0))
            rho = self.RFlaws[ilaw].correlation[i,j]
            self.Ui.tableWidgetCorrelation.setItem(i,j,QTableWidgetItem(str(rho)))

        self.RFlaws[ilaw].correlation[i,j] = rho
        self.RFlaws[ilaw].correlation[j,i] = rho

        if np.linalg.det(self.RFlaws[ilaw].correlation) < 0.:
            print('Determiniant of correlation matrix is negative!')

        self.Ui.tableWidgetCorrelation.setItem(j,i,QTableWidgetItem(str(rho)))
        self.Ui.tableWidgetCorrelation.setItem(i,j,QTableWidgetItem(str(rho)))


    def meanChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].mean[ivar] = float(self.Ui.lineEditMean.text())
        if self.RFlaws[ilaw].mean[ivar] == 0.:
            self.RFlaws[ilaw].CoV[ivar] = 0.0
            self.Ui.lineEditCoV.setText('0.0')

    def sigmaChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].sigma[ivar] = float(self.Ui.lineEditSigma.text())
    def covChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        if self.RFlaws[ilaw].mean[ivar] == 0.:
            self.Ui.lineEditCoV.setText('0.0')  
            print('*** Cannot define CoV for zero-mean field:')
            print('    --> define a non-zero mean first')
        else:
            self.RFlaws[ilaw].CoV[ivar] = float(self.Ui.lineEditCoV.text())

    def perChanged(self):
        self.per = self.Ui.comboBoxPerFile.currentText()

    def varChanged(self):
        self.var = self.Ui.comboBoxVarFile.currentText()


    def pathChanged(self):

        filename = self.Ui.lineEditLagPath.text()

        head,tail = os.path.split(filename)
        if head:
            self.path = head
        else:
            self.path = os.getcwd()

        if tail.upper()[-4:] == '.LAG':
            self.filename = tail[:-4]
        else:
            self.filename = tail


        
        print('>> __.path = "'+self.path+'"')
        print('>> __.filename = "'+self.filename+'"')
        
        self.per = '.f01'
        self.var = '.f02'
        
        self.RFlaws = []
        


    def nuChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].nu[ivar] = float(self.Ui.lineEditNu.text())
    def noiseRatioChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].noiseRatio[ivar] = float(self.Ui.lineEditNoiseRatio.text())
    def constantRatioChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].constantRatio[ivar] = float(self.Ui.lineEditConstantRatio.text())
    def theta1Changed(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        theta1 = float(self.Ui.lineEditTheta1.text())
        self.RFlaws[ilaw].theta[ivar][0] = theta1
        if self.RFlaws[ilaw].isotropic[ivar]:
            self.RFlaws[ilaw].theta[ivar][1] = theta1
            self.RFlaws[ilaw].theta[ivar][2] = theta1
            self.Ui.lineEditTheta2.setText(str(theta1))
            self.Ui.lineEditTheta3.setText(str(theta1))
    def theta2Changed(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].theta[ivar][1] = float(self.Ui.lineEditTheta2.text())
    def theta3Changed(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        self.RFlaws[ilaw].theta[ivar][2] = float(self.Ui.lineEditTheta3.text())


    def marginalChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        mmod = self.Ui.comboBoxMarginal.currentText()
        if len(self.RFlaws) > 0:
            self.RFlaws[ilaw].marginalModel[ivar] = mmod

    def spatialChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        smod = self.Ui.comboBoxSpatial.currentText()

        self.RFlaws[ilaw].spatialModel[ivar] = smod
        if smod == 'Matern':
            self.Ui.lineEditNu.setEnabled(True)
        else:
            self.Ui.lineEditNu.setEnabled(False)

    def periodicChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        perio = self.Ui.checkBoxPeriodic.checkState()

        self.RFlaws[ilaw].periodic[ivar] = perio

    def isotropicChanged(self):
        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        iso = self.Ui.checkBoxIsotropic.checkState()
        theta1 = self.RFlaws[ilaw].theta[ivar][0]
        self.RFlaws[ilaw].isotropic[ivar] = iso

        if iso:
            self.Ui.lineEditTheta2.setEnabled(False)
            self.Ui.lineEditTheta3.setEnabled(False)
            self.Ui.lineEditTheta2.setText(str(theta1))
            self.Ui.lineEditTheta3.setText(str(theta1))
        else:
            self.Ui.lineEditTheta2.setEnabled(True)
            self.Ui.lineEditTheta3.setEnabled(True)


    def setupUi(self):
        
        
        # general
        self.Ui.tabParameters.setEnabled(False)
        self.Ui.tabGenerate.setEnabled(False)
        self.Ui.tabCompute.setEnabled(False)

        self.Ui.lineEditLagPath.setText(os.getcwd())
        
        # load
        self.Ui.lineEditLagPath.editingFinished.connect(self.pathChanged)
        self.Ui.comboBoxPerFile.currentTextChanged.connect(self.perChanged)
        self.Ui.comboBoxVarFile.currentTextChanged.connect(self.varChanged)


        self.Ui.pushButtonLoad.clicked.connect(self.loadFiles)


        # parameters
        self.Ui.comboBoxColaw.currentTextChanged.connect(self.colawChanged)
        self.Ui.comboBoxColaw.currentIndexChanged.connect(self.colawChanged)

        self.Ui.comboBoxVariable.currentTextChanged.connect(self.loadParameters)
        self.Ui.comboBoxVariable.currentIndexChanged.connect(self.loadParameters)

        self.Ui.comboBoxSpatial.currentTextChanged.connect(self.spatialChanged)
        self.Ui.comboBoxSpatial.currentIndexChanged.connect(self.spatialChanged)

        self.Ui.comboBoxMarginal.currentTextChanged.connect(self.marginalChanged)
        self.Ui.comboBoxMarginal.currentIndexChanged.connect(self.marginalChanged)


        self.Ui.tableWidgetCorrelation.currentCellChanged.connect(self.correlChanged)

        self.Ui.lineEditMean.editingFinished.connect(self.meanChanged)
        self.Ui.lineEditSigma.editingFinished.connect(self.sigmaChanged)
        self.Ui.lineEditCoV.editingFinished.connect(self.covChanged)
        self.Ui.lineEditNu.editingFinished.connect(self.nuChanged)
        self.Ui.lineEditNoiseRatio.editingFinished.connect(self.noiseRatioChanged)
        self.Ui.lineEditConstantRatio.editingFinished.connect(self.constantRatioChanged)
        self.Ui.checkBoxIsotropic.stateChanged.connect(self.isotropicChanged)
        self.Ui.checkBoxPeriodic.stateChanged.connect(self.periodicChanged)
        self.Ui.lineEditTheta1.editingFinished.connect(self.theta1Changed)
        self.Ui.lineEditTheta2.editingFinished.connect(self.theta2Changed)
        self.Ui.lineEditTheta3.editingFinished.connect(self.theta3Changed)

        self.Ui.toolButtonExample.clicked.connect(self.make_example)
        
        self.Ui.pushButtonGenerate.clicked.connect(self.generateClicked)   
        self.Ui.pushButtonExport.clicked.connect(self.exportRFstats) 
        self.Ui.pushButtonPlotVar.clicked.connect(self.plot_ip_values_2)
        self.Ui.pushButtonPlotSigma.clicked.connect(self.plot_ip_sigma)

        self.Ui.comboBoxColawPlot.currentIndexChanged.connect(self.colawPlotChanged)
        
        self.Ui.toolButtonLagPath.clicked.connect(self.openBrowser)


        self.Ui.pushButtonCompute.clicked.connect(self.runRFEM)
        
        return
    
    def runRFEM(self):
        import sys
        import subprocess


        loadfilename = self.Ui.lineEditStrategyFiles.text()
        exePath = self.Ui.lineEditLagaminePath.text()

        self.nfield = self.Ui.spinBoxNreal.value()

        for ireal in range(self.nfield):
            
            print('starting realisation ',ireal)

            loadFile = self.path + '/real_%4.4i/'%(ireal) + loadfilename
            dataFile = self.path + '/real_%4.4i/'%(ireal) + self.filename + '.lag'
            subprocess.call([exePath, loadFile, dataFile])
            
    def openBrowser(self):
        fname = QFileDialog.getOpenFileName(QFileDialog(),'Select simulation',self.path,"Text files (*.LAG)")
        
        self.Ui.lineEditLagPath.setText(fname[0])
        self.pathChanged()


    def exportRFstats(self):
        import json
        with open(self.filename+'.json', 'w') as jsonfile:
            for law in self.RFlaws:
                law.export_to_json(jsonfile)

        os.startfile(self.filename+'.json')


    def generateClicked(self):
        self.generateRF_GS()
        self.exportRFstats()
        self.writeRF()
                
    def generateRF(self):
        '''
        
        
        '''
        from scipy.spatial import distance_matrix
        def Matern(dx,nu):
            from scipy.special import gamma,kv

            dx[dx<=0.0] = 1.e-12
            M = (np.sqrt(2*nu)*dx)**nu 
            M *= kv(nu,np.sqrt(2*nu)*dx)
            M *= 2**(1-nu)/gamma(nu)
            return M      

        self.nfield = self.Ui.spinBoxNreal.value()

        xip,yip,zip,elem_per_law = self.extract_IPcoord()

        kvar = 0
        for ilaw,law in enumerate(self.RFlaws):
            IL = self.Ui.colaws_with_RF[ilaw]-1
            npoint = len(xip[IL])

            Irf = []
            
            krf = 0
            if ilaw > 0:
                kvar += self.LTYPE[1][ilaw-1]
            for ivar in range(law.nvariable):
                if (law.sigma[ivar] + law.CoV[ivar]) > 0.:
                    krf += 1
                    Irf.append(ivar)

            rf = np.zeros([krf,self.nfield,npoint])            

            k = -1
            for ivar in range(law.nvariable):
                
                smod = law.spatialModel[ivar]
                mmod = law.marginalModel[ivar]
                nrat = law.noiseRatio[ivar]
                crat = law.constantRatio[ivar]
                the1 = law.theta[ivar][0]
                the2 = law.theta[ivar][1]
                the3 = law.theta[ivar][2]
                nu = law.nu[ivar]
                mu = law.mean[ivar]
                sig = law.sigma[ivar]
                cov = law.CoV[ivar]
                per = law.periodic[ivar]


                if sig + cov > 0.:
                    k += 1

                    if per:
                        
                        # Implementation of 3D has not been done yet

                        m = len(np.unique(xip[IL].round(8)))
                        n = len(np.unique(yip[IL].round(8)))

                        prf = perioRF(m,n,the1,the2,model = smod)

                        R = prf.sample_Z(nreal = self.nfield)

                        for i in range(int(m/2)):
                            for j in range(int(n/2)):
                                ie = int(m/2)*j + i

                                rf[k,:,ie*4 + 0] = R[:,2*m*j + 2*i + 0]
                                rf[k,:,ie*4 + 1] = R[:,2*m*j + 2*i + m + 0]
                                rf[k,:,ie*4 + 2] = R[:,2*m*j + 2*i + m + 1]
                                rf[k,:,ie*4 + 3] = R[:,2*m*j + 2*i + 1]

                    else:

                        xper = (self.CONEC[0,:,:].max(axis = 1)-self.CONEC[0,:,:].min(axis = 1))/2

                        dx = distance_matrix(xip[IL].reshape(-1,1),xip[IL].reshape(-1,1))

                        dy = distance_matrix(yip[IL].reshape(-1,1),yip[IL].reshape(-1,1))

                        dz = distance_matrix(zip[IL].reshape(-1,1),zip[IL].reshape(-1,1))

                        print(xper)
                        if self.periodic:
                            dx[dx>xper[0]] = xper[0]*2-dx[dx>xper[0]] 
                            dy[dy>xper[1]] = xper[1]*2-dy[dy>xper[1]] 

                        dx /= the1
                        dy /= the2
                        dz /= the3
                        
                        tau = np.sqrt(dx**2 + dy**2 + dz**2)

                        del dx
                        del dy
                        del dz
                        print(tau.shape)
    
                        if smod == 'Markov':
                            rho = nrat*np.eye(npoint) + (1-nrat-crat)*np.exp(-2*tau)
                        elif smod == 'Gaussian':
                            rho = nrat*np.eye(npoint) + (1-nrat-crat)*np.exp(-np.pi*tau**2)
                        elif smod == 'Matern':
                            rho = nrat*np.eye(npoint) + (1-nrat-crat)*Matern(tau,nu)
                        del tau

                        try:
                            A = np.linalg.cholesky(rho)
                        except:
                            print('COULD NOT EVALUATE THE CHOLESKY FACTORISATION')
                            print('WILL PERFORM EIGENDECOMPOSITION WITH')
                            
                            if len(rho) > 2000:
                                print('skipping eigendecomposition because of size of matrix')
                                A = np.ones(len(rho))
                            else:
                                L,V = np.linalg.eig(rho)
                                L = np.real(L)
                                V = np.real(V)
                                print(L)
                                L[L<0] = 0
                                A = np.einsum('ik,k,kj->ij',V,np.sqrt(L),V.T)

                        rf[k] = (A @ np.random.normal(size = [npoint,self.nfield])).T
            #
            # cross-correlate random fields using  
            # eigendecomposition of cross-correlation matrix
            #
            corr = self.RFlaws[ilaw].correlation[np.ix_(Irf,Irf)]
            L,V = np.linalg.eig(corr)
            L[L < 0.] = 0.

            A = V @ np.diag(np.sqrt(L)) @ V.T

            rf = np.einsum('ij,jkl->ikl',A,rf)
            #
            k = -1
            for ivar in range(law.nvariable):
                mu = law.mean[ivar]
                sig = law.sigma[ivar]
                cov = law.CoV[ivar]
                mmod = law.marginalModel[ivar]
                nrat = law.noiseRatio[ivar]
                crat = law.constantRatio[ivar]
                if sig + cov > 0.:
                    k += 1
                    #
                    # apply marginal distribution
                    #
                    S = np.sqrt((mu*cov)**2+sig**2)
                    COV = S/mu
                    if mmod == 'Normal':
                        rf[k] *= S
                        rf[k] += mu
                    elif mmod == 'LogNormal':
                        mu_X = np.log(mu/np.sqrt(1+COV**2))
                        S_X = np.sqrt(np.log(1+COV**2))
                        rf[k] = np.exp(mu_X + S_X * rf[k])

                    self.RFlaws[ilaw].RF[ivar] = rf[k]

                    ## put rf in variables
                    kk = 0
                    for ielem in elem_per_law[IL]:
                        nip = self.LNIP[ielem][0]
                        for ip in range(nip):
                            self.VARIN[ielem][ip,kvar+law.iVARIN[ivar]-1] = rf[k,0,kk]
                            kk += 1

                else:
                    self.RFlaws[ilaw].RF[ivar] = np.array([])

                    ## put mean in variables
                    kk = 0
                    for ielem in elem_per_law[IL]:
                        nip = self.LNIP[ielem][0]
                        for ip in range(nip):
                            self.VARIN[ielem][ip,kvar+law.iVARIN[ivar]-1] = mu                            
                            kk += 1
            
    def generateRF_GS(self, SEED=True):
        '''
        

        '''

        IANA = self.per_1['IANA'][0]                                    # Problem type, if equal to 4 then 3D

        self.nfield = self.Ui.spinBoxNreal.value()

        xip,yip,zip,elem_per_law = self.extract_IPcoord()               # extract Integration point coordinates

        kvar = 0
        srf_list = []
        for ilaw,law in enumerate(self.RFlaws):                         # Loop through all laws (mehanical, flow)
            IL = self.Ui.colaws_with_RF[ilaw]-1
            npoint = len(xip[IL])

            Irf = []
            
            krf = 0
            if ilaw > 0:
                kvar += self.LTYPE[1][ilaw-1]
            for ivar in range(law.nvariable):
                if (law.sigma[ivar] + law.CoV[ivar]) > 0.:
                    krf += 1
                    Irf.append(ivar)
            
            rf = np.zeros([krf,self.nfield,npoint])            
            srf_var_list = []
            
            k = -1

            for ivar in range(law.nvariable):

                smod = law.spatialModel[ivar]
                mmod = law.marginalModel[ivar]
                nrat = law.noiseRatio[ivar]
                crat = law.constantRatio[ivar]
                the1 = law.theta[ivar][0] / 2
                the2 = law.theta[ivar][1] / 2
                the3 = law.theta[ivar][2] / 2
                nu = law.nu[ivar]
                mu = law.mean[ivar]
                sig = law.sigma[ivar]
                cov = law.CoV[ivar]
                per = law.periodic[ivar]

                if sig + cov > 0.:
                    
                    seed = MasterRNG(int(datetime.now().strftime("%Y%m%d") + str(ilaw) + (str(ivar))))    # Master seed consists of the date, ilaw and ivar concatenated into one integer
                    seeds = [[int(datetime.now().strftime("%Y%m%d") + str(ilaw) + (str(ivar)))]]

                    k += 1

                    S = np.sqrt((mu*cov)**2+sig**2)

                    if IANA != 4:
                        
                        if smod == 'Markov': 
                            model = gs.Exponential(dim=2, var=S**2, len_scale=[the1,the2])
                        elif smod == 'Gaussian':
                            model = gs.Gaussian(dim=2, var=S**2, len_scale=[the1,the2])

                        srf = gs.SRF(model, mean=mu)
                    
                    elif IANA == 4:
                        if smod == 'Markov':
                            model = gs.Exponential(dim=3, var=S**2, len_scale=[the1,the2,the3])
                        elif smod == 'Gaussian':
                            model = gs.Gaussian(dim=3, var=S**2, len_scale=[the1,the2,the3])

                        srf = gs.SRF(model, mean=mu)
                    
                    if mmod == 'LogNormal':
                        srf.transform('lognormal')

                    if IANA != 4:
                        field = srf((xip[IL],yip[IL]))
                    elif IANA == 4:
                        field = srf((xip[IL],yip[IL],zip[IL]))

                    for ifield in range(self.nfield):
                        # print('SEED: ', SEED)
                        if SEED == False:
                            rf[k,ifield] = field
                        
                        elif SEED == True:
                            iseed = seed()
                            field = srf(seed=iseed)
                            rf[k,ifield] = field
                            seeds.append(iseed)

                    self.RFlaws[ilaw].RF[ivar] = rf[k]
                    self.RFlaws[ilaw].seed[ivar] = seeds
                    ## put rf in variables
                    kk = 0
                    for ielem in elem_per_law[IL]:
                        nip = self.LNIP[ielem][0]
                        for ip in range(nip):
                            self.VARIN[ielem][ip,kvar+law.iVARIN[ivar]-1] = rf[k,0,kk]
                            kk += 1
                    
                else:
                    self.RFlaws[ilaw].RF[ivar] = np.array([])
                    self.RFlaws[ilaw].RF[ivar] = np.array([])

                    ## put mean in variables
                    kk = 0
                    for ielem in elem_per_law[IL]:
                        nip = self.LNIP[ielem][0]
                        for ip in range(nip):
                            self.VARIN[ielem][ip,kvar+law.iVARIN[ivar]-1] = mu                            
                            kk += 1
                
                if sig + cov > 0:
                    srf_var_list.append(deepcopy(srf))
                else:
                    srf_var_list.append([])

            srf_list.append(srf_var_list)

        return srf_list

    def writeRF(self):
        import shutil
        _,_,_,elem_per_law = self.extract_IPcoord()

        for ireal in range(self.nfield):
            kvar = 0
            for ilaw,law in enumerate(self.RFlaws):
                IL = self.Ui.colaws_with_RF[ilaw]-1
                if ilaw > 0:
                    kvar += self.LTYPE[1][ilaw-1]
                for ivar in range(law.nvariable):
                    mu = self.RFlaws[ilaw].mean[ivar]
                    rf = self.RFlaws[ilaw].RF[ivar]
                    ## put rf in variables
                    k = 0
                    for ielem in elem_per_law[IL]:
                        nip = self.LNIP[ielem][0]
                        for ip in range(nip):
                            if len(rf):
                                self.VARIN[ielem][ip,kvar+law.iVARIN[ivar]-1] = rf[ireal,k]
                                k += 1
                            else:
                                self.VARIN[ielem][ip,kvar+law.iVARIN[ivar]-1] = mu
                        

            dir = self.path+'/real_%4.4i/'%(ireal)
            if os.path.exists(dir):
                shutil.rmtree(dir)
            os.makedirs(dir)

            self.write_files(dir = dir)

            extensions = self.Ui.lineEditCopyFiles.text().split(';')
            for ext in extensions:
                f0 = self.path + '/' + self.filename + ext
                f1 = dir + self.filename + ext
                shutil.copy(f0,f1)

            stratf = self.Ui.lineEditStrategyFiles.text().split(';')
            for strategyFile in stratf:
                f0 = self.path + '/' + strategyFile
                f1 = dir + strategyFile
                shutil.copy(f0,f1)
           
    def clickedLagamine(self):
        '''
        Launched Prepro
        
        '''
        print('trying to launch Lagamine')

        if self.DataFileSelected and self.LoadFileSelected:

            if self.Compiler == 'Intel':
                exePath = self.path["LAGAMINE"]
                loadFile = self.path["load"]
                dataFile = self.path["data"]
                subprocess.call([exePath, loadFile, dataFile])
        else:
            msg = QMessageBox()
            msg.setText('First select a DATA and a LOAD file')
            msg.setWindowTitle('Error')
            msg.exec_()
        return
        


    def getParameters(self):
        '''        '''

        ilaw = self.Ui.comboBoxColaw.currentIndex()
        ivar = self.Ui.comboBoxVariable.currentIndex()
        
        self.RFlaws[ilaw].sigma[ivar] = float(self.Ui.lineEditSigma.text()) 
        self.RFlaws[ilaw].CoV[ivar] = float(self.Ui.lineEditCoV.text()) 
        self.RFlaws[ilaw].mean[ivar] = float(self.Ui.lineEditMean.text())

        spatialModel = self.Ui.comboBoxSpatial.currentText()
        if spatialModel == 'Matern':
            self.RFlaws[ilaw].nu[ivar] = float(self.Ui.lineEditNu.text())
            self.Ui.lineEditNu.setEnabled(True)

        self.RFlaws[ilaw].noiseRatio[ivar] = float(self.Ui.lineEditNoiseRatio.text()) 
        self.RFlaws[ilaw].constantRatio[ivar] = float(self.Ui.lineEditConstantRatio.text()) 
        
        if self.Ui.checkBoxIsotropic.checkState() == 0:
            self.Ui.lineEditTheta2.setEnabled(True)
            self.Ui.lineEditTheta3.setEnabled(True)
            self.RFlaws[ilaw].theta[ivar] = [float(self.Ui.lineEditTheta1.text()),
                                             float(self.Ui.lineEditTheta2.text()),
                                             float(self.Ui.lineEditTheta3.text())]
        else:
            theta = float(self.Ui.lineEditTheta1.text())
            self.RFlaws[ilaw].theta[ivar] = [theta]*3 
            self.Ui.lineEditTheta2.setText(str(theta))
            self.Ui.lineEditTheta3.setText(str(theta))
            self.Ui.lineEditTheta2.setEnabled(False)
            self.Ui.lineEditTheta3.setEnabled(False)

        print(self.Ui.plainTextEditCorrelations.toPlainText())

    
    
if __name__ == '__main__':
    
    
    # %matplotlib qt
    
    
    R = Rfem() #filename = r'example/HM_biax_RFEM')
    R.GUI()
