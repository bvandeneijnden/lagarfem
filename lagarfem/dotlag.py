import numpy as np
import matplotlib.pyplot as plt


def sint(s):
    if s.strip() == '':
        return 0
    else: 
        return int(s)


def sfloat(s):
    if s.strip() == '':
        return 0.
    else: 
        return float(s)


class law():
    def __init__(self):
        
        self._type = ''
        self._class = ''
        self._model = ''
        self.model = None


class dotLagFile():
    '''
    Class for handling data (.LAG) files
    '''
    def __init__(self):
        
        self.NDOFlist = [[0,0],                        
                    [1,3],                        
                    [2,4],                        
                    [3,3],                        
                    [4,3],                        
                    [5,5],                        
                    [6,4],                        
                    [7,3],                        
                    [8,6],                        
                    [9,4],                        
                    [10,5],                        
                    [11,0],                        
                    [12,0],                        
                    [13,6],                        
                    [14,6],                        
                    [15,6],                        
                    [16,7],                        
                    [17,9],                        
                    [18,8]]    
        from constitutive_laws import ELA_law,PLASOL_law,LICHA_law, BINDS_law, MOHR_law
        self.lawDict = {0:law,
                        1:ELA_law,
                        30:BINDS_law,
                        72:PLASOL_law,
                        95:LICHA_law,
                        5910:MOHR_law}
        self.elements = []
        self.XYZ = np.zeros([3,0])
        self.NODEF = []

    def readDotImp(self):
        
        A = open(self._fileName[:-4] + '.imp').readlines()
        
        for i,a in enumerate(A):
            if len(a) > 20:
                if a[:20] == ' NODAL DISPLACEMENTS':
                    I = i+3
                    break
        UVW = [[0,0,0]]
        
        for a in A[I:I+self.NUMNP]:
            UVW.append([float(a[21:36]),
                        float(a[45:60]),
                        0. ])
        
        self.UVW = np.array(UVW[1:])
        
    def plotDeformed(self,mult):
        import matplotlib.pyplot as plt
        
        X = self.XYZ[0] + self.UVW[:,0] * mult
        Y = self.XYZ[1] + self.UVW[:,1] * mult
        
        plt.plot(X,Y,'.r')
                    

    def readDataFile(self,fileName):
        
        self._fileName = fileName                
        
        print('reading from ',fileName)
        
        fid = open(fileName,'r+')
        #
        # READ FIRST LINE
        #
        line = fid.readline().rstrip()
        self.TITLE = line
        self.I99999 = 0
        if len(line) > 72:
            try:
                self.TITLE, = line[:72]
                self.I99999 = int(line[72:] or '0')
            except: 
                print('*PROBLEMS IN READING TITLE AND I99999')
                print('*CONTINUING WITH :')
                print('* --> TITLE = "',self.TITLE,'"')
                print('* --> I99999= ',self.I99999)
        #
        # READ SECOND LINE
        #
        line = fid.readline().rstrip() + ' '*80
        
        if self.I99999 == 0:
            self.NTANA = int(line[  : 5].strip() or '0')
            self.IANA  = int(line[ 5:10].strip() or '0')
            self.NUMNP = int(line[10:15].strip() or '0')
            self.NELTP = int(line[15:20].strip() or '0')
            self.MELEM = int(line[20:25].strip() or '0')
            self.NDISP = int(line[25:30].strip() or '0')
            self.NFOUN = int(line[30:35].strip() or '0')
            self.NSEGT = int(line[35:40].strip() or '0')
            self.IPILO = int(line[40:45].strip() or '0')
            self.NLAW  = int(line[45:50].strip() or '0')
            self.IRICE = int(line[50:55].strip() or '0')
            self.ICGEO = int(line[55:60].strip() or '0')
            self.IERRO = int(line[60:65].strip() or '0')
            self.ICRIT = int(line[65:70].strip() or '0')
        else:
            self.NTANA = int(line[  : 5].strip() or '0')
            self.IANA  = int(line[ 5:10].strip() or '0')
            self.NUMNP = int(line[10:20].strip() or '0')
            self.NELTP = int(line[20:25].strip() or '0')
            self.MELEM = int(line[25:35].strip() or '0')
            self.NDISP = int(line[35:40].strip() or '0')
            self.NFOUN = int(line[40:45].strip() or '0')
            self.NSEGT = int(line[45:50].strip() or '0')
            self.IPILO = int(line[50:55].strip() or '0')
            self.NLAW  = int(line[55:60].strip() or '0')
            self.IRICE = int(line[60:65].strip() or '0')
            self.ICGEO = int(line[65:70].strip() or '0')
            self.IERRO = int(line[70:75].strip() or '0')
            self.ICRIT = int(line[75:80].strip() or '0')
            
            
        self.NDOFN = self.NDOFlist[self.NTANA][1]            
        #
        # READ THIRD LINE
        #
        line = fid.readline().rstrip() + ' '*75
        
        self.IRENU = int(line[ 0: 5].strip() or '0')
        self.IGRAV = int(line[ 5:10].strip() or '0')
        self.IDMAT = int(line[10:15].strip() or '0')
        self.MPARA = int(line[15:20].strip() or '0')
        self.MVARI = int(line[20:25].strip() or '0')
        self.MSIGP = int(line[25:30].strip() or '0')
        self.MPARI = int(line[30:35].strip() or '0')
        self.IMDIS = int(line[35:40].strip() or '0')
        self.IPIFO = int(line[40:45].strip() or '0')
        self.ICEVO = int(line[45:50].strip() or '0')
        self.ITREE = int(line[50:55].strip() or '0')
        self.ICOUP = int(line[55:60].strip() or '0')
        self.ILIEN = int(line[60:65].strip() or '0')
        self.NDUPL = int(line[65:70].strip() or '0')
        self.INCFO = int(line[70:75].strip() or '0')
        #
        # READ FOURTH LINE (dynamic analysis)
        #
        line = fid.readline().rstrip() + ' '*20
        
        self.NMASC = int(line[ 0: 5].strip() or '0')
        self.NDAMC = int(line[ 5:10].strip() or '0')
        self.NVEL0 = int(line[10:15].strip() or '0')
        self.NACC0 = int(line[15:20].strip() or '0')
        
        #
        # READ FIFTH LINE (metallurgic analysis)
        #
        line = fid.readline().rstrip() + ' '*25
        
        self.NLAWM = int(line[ 0: 5].strip() or '0')
        self.MTPAM = int(line[ 5:10].strip() or '0')
        self.MCPAM = int(line[10:15].strip() or '0')
        self.MDPAM = int(line[15:20].strip() or '0')
        self.META  = int(line[20:25].strip() or '0')
    
        n99 = 5
        if self.I99999 != 0:
            n99 = 10
        
           
        line = fid.readline()
        while len(line)>0:
            l = line.rstrip()
            
            if l.upper() == 'NODES':
                print('NODES')
                self.XYZ = np.zeros([self.NDOFN,self.NUMNP])
                self.XYZ[:,0] = np.nan
                for ip in range(self.NUMNP):
                    fid.read(n99)
                    for i in range(self.NDOFN):
                        self.XYZ[i,ip] = float(fid.read(10))
                    next(fid)
    
            if l.upper() == 'RENUM':
                print('RENUM')
                self.IRENU = 1
                l = fid.readline().rstrip() + ' '*60
                
                if self.I99999 == 0:
                    self.ISTAR = sint(l[:5])
                    self.KZONE = sint(l[5:10])
                    self.IPRIN = sint(l[10:15])
                    self.IFOND = sint(l[15:20])
                    self.ITYREN = sint(l[20:25])
                    self.BDFAC = sfloat(l[25:35])
                else:
                    self.ISTAR = int(l[:5] or '0')
                    self.KZONE = int(l[5:10] or '0')
                    self.IPRIN = int(l[10:15] or '0')
                    self.IFOND = int(l[15:20] or '0')
                    self.ITYREN = int(l[20:25] or '0')
                    self.BDFAC = float(l[25:35] or '0.')
    
            if l.upper() == 'FIXED':
                print('FIXED')
                self.NODEF = [[] for _ in range(self.NDOFN)]
                l = fid.readline().rstrip()
                while len(l) > 0:
                    l = [int(i)for i in l.split()]
                    idof = l[0]-1
                    self.NODEF[idof].extend(l[1:])
                    l = fid.readline().rstrip()

            if l.upper() == 'DISPL':
                print('DISPL')
                self.DISPL = []
                l = fid.readline().rstrip() 
                while l[0] not in ['F','I']:
                    l += ' '*60
                    d = [int(l[:5])]
                    for i in range(6):
                        d.append(l[5 + 10*i:15 + 10*i])
                    l = fid.readline().rstrip()
            
            if l.upper() == 'FORCES':
                next(fid)
                pass

    
            if l.upper() == 'COLAW':
                print('COLAW')
                self.laws = []
                for il in range(self.NLAW):
                    self.laws.append(law())
                    l = fid.readline().rstrip() + ' '*70
                    IL = int(l[0:5] or '0')
                    ITYPE = int(l[5:10] or '0')
                    print('--->',IL,ITYPE)
                    self.laws[il].model = self.lawDict[ITYPE]()
                    self.laws[il].model.COMMNT = l[10:]
                    self.laws[il].model.IL = IL
                    self.laws[il].model.readLag(fid)

            if l.upper() == 'BINDS':
                print('BINDS')
                class binds():
                    pass
                P = binds()
                P.elementName = 'BINDS'
                P.elementType = 99
                l = fid.readline().strip() + ' '*15
                P.NELEM = int(l[:5].rstrip() or '0')
                
                P.LMATE = [None]*P.NELEM
                P.NNODE = [None]*P.NELEM
                P.INDISO = [None]*P.NELEM
                
                P.NODES = [None]*P.NELEM
                P.IDOF = [None]*P.NELEM
                P.INDIS = [None]*P.NELEM
                
                for ie in range(P.NELEM):
                    P.LMATE[ie] = int(fid.read(5))
                    P.NNODE[ie] = int(fid.read(5))
                    P.INDISO[ie] = int(fid.read(5))
                    next(fid)
                    
                    P.NODES[ie] = [0]*P.NNODE[ie]
                    P.IDOF[ie] = [0]*P.NNODE[ie]
                    P.INDIS[ie] = [0]*P.NNODE[ie]
                    
                    for ind in range(P.NNODE[ie]):
                        P.NODES[ie][ind] = int(fid.read(5))
                        P.IDOF[ie][ind] = int(fid.read(5))
                        P.INDIS[ie][ind] = int(fid.read(5))
                    next(fid)
                
                self.elements.append(P)                


            if l.upper() == 'PLXLS':
                print('PLXLS')
                class plxls:
                    pass
                P = plxls()
                #
                # FIRST LINE
                #
                P.elementName = 'PLXLS'
                P.elementType = 9
                l = fid.readline().rstrip() + ' '*15
                P.NELEM = int(l[:5].rstrip() or '0')
                P.ISPMAS = int(l[5:10].rstrip() or '0')                                
                P.INSIG = int(l[10:15].rstrip() or '0')                                
                
                if P.ISPMAS == 1:
                    P.SPEMASS = float(fid.read(10))
                    next(fid)
                if P.INSIG == 1 or P.INSIG == 2:
                    l = fid.readline().rstrip() + ' '*50
                    P.SIGY0 = float(l[:10].rstrip() )
                    P.DSIGY = float(l[10:20].rstrip() )
                    P.AK0X = float(l[20:30].rstrip() )
                    P.AK0Z = float(l[30:40].rstrip() )
                
                P.NNODE = [None]*P.NELEM
                P.NINTE = [None]*P.NELEM
                P.LMATE = [None]*P.NELEM
                P.NODES = [None]*P.NELEM
                for ie in range(P.NELEM):
                    P.NNODE[ie] = int(fid.read(5))
                    P.NINTE[ie] = int(fid.read(5))
                    P.LMATE[ie] = int(fid.read(5))
                    next(fid)
                    P.NODES[ie] = [int(fid.read(5)) for i in range(P.NNODE[ie])]
                    next(fid)                    
                
                self.elements.append(P)                
                
            if l.upper() == 'LICHA':
                class licha:
                    pass
                P = licha()
                #
                # FIRST LINE
                #
                P.elementName = 'LICHA'
                P.elementType = 0
                l = fid.readline().rstrip() + ' '*10
                P.NELEM = int(l[:5].rstrip() or '0')
                P.NGROUL = int(l[5:10].rstrip() or '0')                                
                #
                # ELEMENTS
                #
                P.NINTE = [None]*P.NELEM
                P.LMATE = [None]*P.NELEM
                P.NODES = [None]*P.NELEM
                for ie in range(P.NELEM):
                    P.NINTE[ie] = int(fid.read(5))
                    P.LMATE[ie] = int(fid.read(5))
                    next(fid)
                    l = fid.readline().rstrip()
                    P.NODES[ie] = [int(l[i:i+5]) for i in range(0,len(l),5)]

                self.elements.append(P)                
    
            line = fid.readline()
            
        fid.close()

    def writeDataFile(self,fileName):

        fid = open(fileName,'w+')
        #
        # WRITE FIRST LINE
        #
        fid.write('%72s'%(self.TITLE))
        if self.I99999 != 0:
            fid.write('%5i'%(self.I99999))
        fid.write('\n')
        #
        # WRITE SECOND LINE
        #
        if self.I99999 == 0:
            for I in [self.NTANA,self.IANA ,self.NUMNP,self.NELTP,self.MELEM,
                      self.NDISP,self.NFOUN,self.NSEGT,self.IPILO,self.NLAW ,
                      self.IRICE,self.ICGEO,self.IERRO,self.ICRIT
                      ]:
                fid.write('%5i'%(I))
            fid.write('\n')

        else:
            fid.write('%5i'%(self.NTANA ))
            fid.write('%5i'%(self.IANA  ))
            fid.write('%10i'%(self.NUMNP))
            fid.write('%5i'%(self.NELTP ))
            fid.write('%10i'%(self.MELEM))
            fid.write('%5i'%(self.NDISP ))
            fid.write('%5i'%(self.NFOUN ))
            fid.write('%5i'%(self.NSEGT ))
            fid.write('%5i'%(self.IPILO ))
            fid.write('%5i'%(self.NLAW  ))
            fid.write('%5i'%(self.IRICE ))
            fid.write('%5i'%(self.ICGEO ))
            fid.write('%5i'%(self.IERRO ))
            fid.write('%5i\n'%(self.ICRIT ))
        #
        # WRITE THIRD LINE
        #
        for I in [self.IRENU,self.IGRAV,self.IDMAT,self.MPARA,self.MVARI,
                  self.MSIGP,self.MPARI,self.IMDIS,self.IPIFO,self.ICEVO,
                  self.ITREE,self.ICOUP,self.ILIEN,self.NDUPL,self.INCFO
                  ]:
            if I == 0:
                fid.write(' '*5)
            else:
                fid.write('%5i'%(I))
        fid.write('\n')
        
        #
        # WRITE FOURTH LINE (dynamic analysis)
        #
        for I in [self.NMASC,self.NDAMC,self.NVEL0,self.NACC0
                  ]:
            if I == 0:
                fid.write(' '*5)
            else:
                fid.write('%5i'%(I))
        fid.write('\n')

        
        #
        # WRITE FIFTH LINE (metallurgic analysis)
        #
        for I in [self.NLAWM,self.MTPAM,self.MCPAM,self.MDPAM,self.META
                  ]:
            if I == 0:
                fid.write(' '*5)
            else:
                fid.write('%5i'%(I))
        fid.write('\n')

        if True:
            fid.write('NODES\n')
            if self.I99999 == 0:
                for i in range(self.XYZ.shape[1]):
                    fid.write('%5i%10.5G%10.5G%10.5G\n'%(i+1,*self.XYZ[:,i]))
            else:
                for i in range(self.XYZ.shape[1]):
                    fid.write('%10i%10G%10G%10G\n'%(i+1,*self.XYZ[:,i]))
        
        if self.IRENU == 1:
            fid.write('RENUM\n')
            if self.I99999 == 0:
                fid.write('%5i%5i%5i%5i%5i%10.5G\n'%(self.ISTAR,
                                                     self.KZONE,
                                                     self.IPRIN,
                                                     self.IFOND,
                                                     self.ITYREN,
                                                     self.BDFAC))
            else:
                fid.write('%10i%10i%10i%10i%10i%10.5G\n'%(self.ISTAR,
                                                          self.KZONE,
                                                          self.IPRIN,
                                                          self.IFOND,
                                                          self.ITYREN,
                                                          self.BDFAC))
            
            

        fid.write('FIXED')
        for j,F in enumerate(self.NODEF):
            for i,f in enumerate(F):
                if np.mod(i,13) == 0:
                    fid.write('\n%5i'%(j+1))
                fid.write('%5i'%(f))
        fid.write('\n\n')

        if self.NDISP > 0:
            fid.write('DISPL\n')
            for F in self.DISPL:
                if self.I99999 == 0:
                    fid.write('%5i%10.5G%10.5G%10.5G%10.5G%10.5G%10.5G\n'%(F[0],*F[1:]))
                else:
                    fid.write('%10i%10G%10G%10G%10G%10G%10G\n'%(F[0],*F[1:]))
            # fid.write('\n')
                


        fid.write('FORCE')
        fid.write('\n\n')
            
        fid.write('COLAW\n')
        if len(self.laws) == 0:
            fid.write('%5i%5i\n'%(1,1))
        else:
            for i,l in enumerate(self.laws):
                print('PRINTING LAW ',i)
                l.model.IL = i+1
                l.model.WriteLag(fid)

        #fid.write('\n\n')


        for E in self.elements:
            if E.elementName == 'BINDS':
                fid.write('%s\n'%(E.elementName))
                fid.write('%5i\n'%(E.NELEM))
                for ie in range(E.NELEM):
                    fid.write('%5i%5i%5i\n'%(E.LMATE[ie], E.NNODE[ie], E.INDISO[ie]))
                    for ind in range(E.NNODE[ie]):
                        fid.write('%5i%5i%5i'%(E.NODES[ie][ind],E.IDOF[ie][ind],E.INDIS[ie][ind]))
                    fid.write('\n')
                    
            elif E.elementName == 'LICHA':
                fid.write('%s\n'%(E.elementName))
                fid.write('%5i%5i\n'%(E.NELEM,E.NGROUL))
                
                for ie in range(E.NELEM):
                    fid.write('%5i%5i\n'%(E.NINTE[ie],E.LMATE[ie]))
                    for p in E.NODES[ie]:
                        fid.write('%5i'%(p))
                    fid.write('\n')
            elif E.elementName == 'PLXLS':
                fid.write('%s\n'%(E.elementName))
                fid.write('%5i%5i%5i\n'%(E.NELEM,E.ISPMAS,E.INSIG))
                if E.ISPMAS == 1:
                    fid.write('%10g\n'%(E.SPEMAS))
                if E.INSIG == 1 or E.INSIG == 2:
                    fid.write('%10g'  %(E.SIGY0))
                    fid.write('%10g'  %(E.DSIGY))
                    fid.write('%10g'  %(E.AK0X ))
                    fid.write('%10g%10g\n'%(E.AK0Z,0. ))
    
                for i in range(E.NELEM):
                    fid.write('%5i'  %(E.NNODE[i]))
                    fid.write('%5i'  %(E.NINTE[i]))
                    fid.write('%5i\n'%(E.LMATE[i]))
                    if self.I99999 == 0:
                        for e in E.NODES[i]:
                            fid.write('%5i'%(e))
                    else:
                        for e in E.NODES[i]:
                            fid.write('%10i'%(e))
                    fid.write('\n')
            else:
                print('THIS ELEMENT HAS NOT BEEN IMPLEMENTED YET,\
                      PLEASE TAKE A MOMENT TO DO THE IMPLEMENTATION IN "dotlag.py"')

        fid.close()

    def plotMesh(self,ax = None):
        
        print('in LAG.plotMesh')
        if ax == None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        
        for eltype in self.elements:
            x = []
            y = []
            if eltype.elementType == 99:
                for nodes in eltype.NODES:
                        #for node in nodes[:-1]:
                        for node in nodes[:2]: # leave out ghost nodes
                            x.append(self.XYZ[0,node-1])
                            y.append(self.XYZ[1,node-1])
                        x.append(np.nan)
                        y.append(np.nan)


                ax.plot(x,y,'o:',color = 'tab:red',linewidth = 0.1,markersize = 5, fillstyle = 'none')      
            else:
                for nodes in eltype.NODES:
                    for node in nodes:
                        x.append(self.XYZ[0,node-1])
                        y.append(self.XYZ[1,node-1])
                    x.append(self.XYZ[0,nodes[0]-1])
                    y.append(self.XYZ[1,nodes[0]-1])
                    x.append(np.nan)
                    y.append(np.nan)
                
                ax.plot(x,y,'.-k',linewidth = 0.5,markersize = 5)      
        
        name = self.TITLE
        if name == '':
            name = self._fileName.split('/')[-1]
        
        plt.title('mesh %s'%(name))
        plt.show()        
        
         
    
            