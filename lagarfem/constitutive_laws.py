
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import numpy as np

def sint(s):
    if s.strip() == '':
        return 0
    else: 
        return int(s)

def sfloat(s):
    if s.strip() == '':
        return 0.
    else: 
        return float(s)


class MOHR_law():
    ''' Class for internal friction law EP-MOHRC'''        
    def __init__(self,lawnumber = 0):

        self.name: str = 'PLASOL'

        self.IL: int = lawnumber
        self.ITYPE: int = 591
        self.COMMNT: str = ''

        self.NINTV: int = 10
        self.ISOL: int = 0
        self.ICBIF = 0
        self.IECPS = 0          # 0/1
        self.KMETH = 2          # 2/3
        
        self.E = 1000.
        self.ANU = 0.2
        self.PSI = 0.
        self.RHO = 20.
        self.DIV = 0.005
        self.PHMPS = 0.
        
        self.PHI0 = 30.
        self.PHIF = 30.
        self.BPHI = 0.
        
        self.COH0 = 10.
        self.COHF = 10.
        self.BCOH = 0.

    def readLag(self,fid):
        #
        # READ 1ST LINE
        #
        l = fid.readline().rstrip() + ' '*45
        if len(l)>0:
            self.NINTV = sint(l[0:5])
            self.ISOL = sint(l[5:10])
            self.ICBIF = sint(l[10:15])
            self.IECPS = sint(l[25:30])
            self.KMETH = sint(l[30:35])
        #
        # SECOND LINE
        #
        l = fid.readline().rstrip() + ' '*70
        self.E = sfloat(l[0:10])
        self.ANU = sfloat(l[10:20])
        self.PSI = sfloat(l[20:30])
        self.RHO = sfloat(l[40:50])
        self.DIV = sfloat(l[50:60])
        self.PHMPS = sfloat(l[60:70])
        #
        # THIRD LINE
        #
        l = fid.readline().rstrip() + ' '*70
        self.PHI0 = sfloat(l[0:10])
        self.PHIF = sfloat(l[10:20]) 
        self.BPHI = sfloat(l[20:30])
        #
        # FOURTH LINE
        #
        l = fid.readline().rstrip() + ' '*70
        self.COH0  = sfloat(l[0:10])
        self.COHF  = sfloat(l[10:20])
        self.BCOH  = sfloat(l[20:30])

        #l = fid.readline().rstrip()
        #if len(l) >=30:
        #    self.E = float(l[:10].rstrip() or 0.)
        #    self.ANU = float(l[10:20].rstrip() or 0.)
        #    self.RHO = float(l[20:30].rstrip() or 0.)
        #else:
        #    print('data missin in law ELA-law ',self.IL)
        

    def GUI(self):
        pass

    def WriteLag(self,fileID = None):
        if fileID == None:
            print('# data_file.LAG\n:\n:\n:')
            print('%5i%5i%60s'%(self.IL,self.ITYPE,self.COMMNT))
            print('%5i%5i'%(self.ISOL,self.NINTV))
        else:
            fileID.write('%5i%5i%60s\n'%(self.IL,self.ITYPE,self.COMMNT))
            
            fileID.write('%5i'%(self.NINTV))
            fileID.write('%5i'%(self.ISOL ))
            fileID.write('%5i'%(self.ICBIF))
            fileID.write('%5i'%(self.IECPS))
            fileID.write('%5i'%(self.KMETH))
            #
            # SECOND LINE
            #
            fileID.write('%10g'%(self.E))
            fileID.write('%10g'%(self.ANU))
            fileID.write('%10g'%(self.PSI))
            fileID.write('%10g'%(self.RHO))
            fileID.write('%10g'%(self.DIV))
            fileID.write('%10g\n'%(self.PHMPS))
            #
            # THIRD LINE
            #
            fileID.write('%10g'%(self.PHI0 ))
            fileID.write('%10g'%(self.PHIF ))
            fileID.write('%10g'%(self.BPHI ))
            #
            # FOURTH LINE
            #
            fileID.write('%10g'%(self.COH0 ))
            fileID.write('%10g'%(self.COHF ))
            fileID.write('%10g'%(self.BCOH ))

class PLASOL_law():
    ''' Class for internal friction law PLASOL'''        
    def __init__(self,lawnumber = 0):

        self.name: str = 'PLASOL'

        self.IL: int = lawnumber
        self.ITYPE: int = 72
        self.COMMNT: str = ''

        self.NINTV: int = 10
        self.ISOL: int = 0
        self.ICBIF = 0
        self.ILODEF = 1
        self.ILODEG = 1         # 1/2
        self.IECPS = 0          # 0/1
        self.KMETH = 2          # 2/3
        self.IREDUC = 0         # 0/1
        self.ICOCA = 0          # 0/1
        
        self.E = 1000.
        self.ANU = 0.2
        self.PSIC = 0.
        self.PSIE = 0.
        self.RHO = 20.
        self.DIV = 0.005
        self.PHMPS = 0.
        
        self.PHIC0 = 30.
        self.PHICF = 30.
        self.BPHI = 0.
        self.PHIE0 = 30.
        self.PHIEF = 30.
        self.AN = -0.229
        self.DECPHI = 0.
        
        self.COH0 = 10.
        self.COHF = 10.
        self.BCOH = 0.
        self.BIOPT = 0.
        self.AK1 = 0.
        self.AK2 = 0.
        self.DECCOH = 0.

    def readLag(self,fid):
        #
        # READ 1ST LINE
        #
        l = fid.readline().rstrip() + ' '*45
        if len(l)>0:
            self.NINTV = int(l[0:5] or '0')
            self.ISOL = int(l[5:10] or '0')
            self.ICBIF = int(l[10:15] or '0')
            self.ILODEF = int(l[15:20] or '0')
            self.ILODEG = int(l[20:25] or '0')
            self.IECPS = int(l[25:30] or '0')
            self.KMETH = int(l[30:35] or '0')
            self.IREDUC = int(l[35:40] or '0')
            self.ICOCA = int(l[40:45] or '0')

        #
        # SECOND LINE
        #
        l = fid.readline().rstrip() + ' '*70
        self.E = float(l[0:10])
        self.ANU = float(l[10:20])
        self.PSIC = float(l[20:30])
        self.PSIE = float(l[30:40])
        self.RHO = float(l[40:50])
        self.DIV = float(l[50:60])
        self.PHMPS = float(l[60:70])
        #
        # THIRD LINE
        #
        l = fid.readline().rstrip() + ' '*70
        self.PHIC0 = float(l[0:10])
        self.PHICF = float(l[10:20]) 
        self.BPHI = float(l[20:30])
        self.PHIE0 = float(l[30:40])
        self.PHIEF = float(l[40:50])
        self.AN = float(l[50:60])
        self.DECPHI = float(l[60:70])
        #
        # FOURTH LINE
        #
        l = fid.readline().rstrip() + ' '*70
        self.COH0  = float(l[0:10])
        self.COHF  = float(l[10:20])
        self.BCOH  = float(l[20:30])
        self.BIOPT  = float(l[30:40])
        self.AK1  = float(l[40:50])
        self.AK2  = float(l[50:60])
        self.DECCOH = float(l[60:70])



            #l = fid.readline().rstrip()
            #if len(l) >=30:
            #    self.E = float(l[:10].rstrip() or 0.)
            #    self.ANU = float(l[10:20].rstrip() or 0.)
            #    self.RHO = float(l[20:30].rstrip() or 0.)
            #else:
            #    print('data missin in law ELA-law ',self.IL)
        

    def GUI(self):
        from ui_forms.Ui_InputPLASOL import Ui_DialogPlasol

        if not QtWidgets.QApplication.instance():
            #app = QtWidgets.QApplication(sys.argv)
            QtWidgets.QApplication(sys.argv)
        else:
            #app = QtWidgets.QApplication.instance()
            QtWidgets.QApplication.instance()

        self.DialogPlasol = QtWidgets.QDialog()
        mainWin = Ui_DialogPlasol()
        mainWin.setupUi(self.DialogPlasol)
        
        mainWin.lineEditE.setText('%g'%(self.E))
        mainWin.lineEditANU.setText('%g'%(self.ANU))
        mainWin.lineEditRHO.setText('%g'%(self.RHO))
        mainWin.lineEditNINTV.setText('%i'%(self.NINTV))
        mainWin.lineEditCOMMNT.setText(self.COMMNT)
        
        mainWin.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).clicked.connect(self.DialogPlasol.accept)
        mainWin.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).clicked.connect(self.DialogPlasol.reject)
        
        self.DialogPlasol.show()
        rsp = self.DialogPlasol.exec_()
        
        print('rsp:',rsp)
        if rsp == QtWidgets.QDialog.Accepted:
        
            self.COMMNT = mainWin.lineEditCOMMNT.text()

            print('PLASOL updating first row')
            self.NINTV: int(mainWin.lineEditNINTV.text())
            self.ISOL = 0
            
            self.ICBIF = 0
            if mainWin.checkBoxICBIF.isChecked():
                self.ICBIF = 1
                
            self.ILODEF = 1
            if mainWin.radioButtonILODEF_2.isChecked():
                self.ILODEF = 2
            self.ILODEG = 1         # 1/2
            if mainWin.radioButtonILODEF_2.isChecked():
                self.ILODEG = 2

            self.IECPS = 0          # 0/1
            self.KMETH = 2          # 2/3
            self.IREDUC = 0         # 0/1
            self.ICOCA = 0          # 0/1
            
            self.E = float(mainWin.lineEditE.text() or '0')
            self.ANU = float(mainWin.lineEditANU.text() or '0')
            self.PSIC = float(mainWin.lineEditPSIC.text() or '0')
            self.PSIE = float(mainWin.lineEditPSIE.text() or '0')
            self.RHO = float(mainWin.lineEditRHO.text() or '0')
            self.DIV = float(mainWin.lineEditDIV.text() or '0')
            self.PHMPS = float(mainWin.lineEditPHMPS.text() or '0')
            
            print('PLASOL updating third row')
            self.PHIC0 = float(mainWin.lineEditPHIC0.text() or '0')
            self.PHICF = float(mainWin.lineEditPHICF.text() or '0')
            self.BPHI = float(mainWin.lineEditBPHI.text() or '0')
            self.PHIE0 = float(mainWin.lineEditPHIE0.text() or '0')
            self.PHIEF = float(mainWin.lineEditPHIEF.text() or '0')
            self.AN = float(mainWin.lineEditAN.text() or '0')
            self.DECPHI = 0. #float(mainWin.lineEditDECPHI.text())
            
            print('PLASOL updating fourth row')
            self.COH0 = float(mainWin.lineEditCOH0.text() or '0')
            self.COHF = float(mainWin.lineEditCOHF.text() or '0')
            self.BCOH = float(mainWin.lineEditBCOH.text() or '0')
            #self.BIOPT = float(mainWin.lineEditBIOPT.text() or '0')
            self.AK1 = float(mainWin.lineEditAK1.text() or '0')
            self.AK2 = float(mainWin.lineEditAK2.text() or '0')
            self.DECCOH = float(mainWin.lineEditDECCOH.text() or '0')



    def WriteLag(self,fileID = None):
        if fileID == None:
            print('# data_file.LAG\n:\n:\n:')
            print('%5i%5i%60s'%(self.IL,self.ITYPE,self.COMMNT))
            print('%5i%5i'%(self.ISOL,self.NINTV))
        else:
            fileID.write('%5i%5i%60s\n'%(self.IL,self.ITYPE,self.COMMNT))
            
            fileID.write('%5i'%(self.NINTV))
            fileID.write('%5i'%(self.ISOL ))
            fileID.write('%5i'%(self.ICBIF))
            fileID.write('%5i'%(self.ILODEF))
            fileID.write('%5i'%(self.ILODEG))
            fileID.write('%5i'%(self.IECPS))
            fileID.write('%5i'%(self.KMETH))
            fileID.write('%5i'%(self.IREDUC))
            fileID.write('%5i\n'%(self.ICOCA))
            #
            # SECOND LINE
            #
            fileID.write('%10g'%(self.E))
            fileID.write('%10g'%(self.ANU))
            fileID.write('%10g'%(self.PSIC))
            fileID.write('%10g'%(self.PSIE))
            fileID.write('%10g'%(self.RHO))
            fileID.write('%10g'%(self.DIV))
            fileID.write('%10g\n'%(self.PHMPS))
            #
            # THIRD LINE
            #
            fileID.write('%10g'%(self.PHIC0 ))
            fileID.write('%10g'%(self.PHICF ))
            fileID.write('%10g'%(self.BPHI ))
            fileID.write('%10g'%(self.PHIE0 ))
            fileID.write('%10g'%(self.PHIEF ))
            fileID.write('%10g'%(self.AN )) 
            fileID.write('%10g\n'%(self.DECPHI ))
            #
            # FOURTH LINE
            #
            fileID.write('%10g'%(self.COH0 ))
            fileID.write('%10g'%(self.COHF ))
            fileID.write('%10g'%(self.BCOH ))
            fileID.write('%10g'%(self.BIOPT ))
            fileID.write('%10g'%(self.AK1 ))
            fileID.write('%10g'%(self.AK2 )) 
            fileID.write('%10g\n'%(self.DECCOH ))
            

class ELA_law():
    """ class for elastic constitutive law """
    def __init__(self,lawnumber=0):
        self.name: str = 'ELA'
        self.ISOL: int = 0
        self.E: float = 1000.
        self.ANU: float = 0.25
        self.RHO: float = 20.
        self.NINTV: int = 10
        self.COMMNT: str = ''
        self.IL: int = lawnumber
        self.ITYPE: int = 1

        
        
    def GUI(self):
        from ui_forms.Ui_InputELA import Ui_DialogEla

        if not QtWidgets.QApplication.instance():
            #app = QtWidgets.QApplication(sys.argv)
            QtWidgets.QApplication(sys.argv)
        else:
            #app = QtWidgets.QApplication.instance()
            QtWidgets.QApplication.instance()

        DialogEla = QtWidgets.QDialog()
        mainWin = Ui_DialogEla()
        mainWin.setupUi(DialogEla)
        
        mainWin.lineEditE.setText('%g'%(self.E))
        mainWin.lineEditNu.setText('%g'%(self.ANU))
        mainWin.lineEditRho.setText('%g'%(self.RHO))
        mainWin.lineEditN.setText('%i'%(self.NINTV))
        mainWin.lineEditC.setText(self.COMMNT)
        mainWin.lineEditName.setText(self.name)
        
        #mainWin.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).clicked.connect(QtWidgets.QApplication.quit)
        #mainWin.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).clicked.connect(QtWidgets.QApplication.quit)
        DialogEla.show()
        rsp = DialogEla.exec_()
        
        print('rsp:',rsp)
        if rsp == QtWidgets.QDialog.Accepted:
        
            self.name = mainWin.lineEditName.text()
            self.E = float(mainWin.lineEditE.text())
            self.ANU = float(mainWin.lineEditNu.text())
            self.RHO = float(mainWin.lineEditRho.text())
            self.NINTV = int(mainWin.lineEditN.text())
            self.COMMNT = mainWin.lineEditC.text()
        
    def WriteLag(self,fileID = None):
        if fileID == None:
            print('# data_file.LAG\n:\n:\n:')
            print('%5i%5i%60s'%(self.IL,self.ITYPE,self.COMMNT))
            print('%5i%5i'%(self.ISOL,self.NINTV))
            print('%10g%10g%10g\n:\n:\n:'%(self.E,self.ANU,self.RHO))
        else:
            fileID.write('%5i%5i%60s\n'%(self.IL,1,self.COMMNT))
            fileID.write('%5i%5i\n'%(self.ISOL,self.NINTV))
            fileID.write('%10g%10g%10g\n'%(self.E,self.ANU,self.RHO))
    
    def readLag(self,fid):
        l = fid.readline().rstrip()
        if len(l)>0:
            self.ISOL = int(l[:5])
            l = fid.readline().rstrip()
            if len(l) >=30:
                self.E = float(l[:10].rstrip() or 0.)
                self.ANU = float(l[10:20].rstrip() or 0.)
                self.RHO = float(l[20:30].rstrip() or 0.)
            else:
                print('data missin in law ELA-law ',self.IL)
        

class LICHA_law():
    """ class for elastic constitutive law """
    def __init__(self,lawnumber=0):
        self.name: str = 'LICHA'
        self.IL: int = lawnumber
        self.ITYPE: int = 95
        self.COMMNT: str = ''


        self.IVAL:int = 1
        self.IMLIC:int = 1
        self.ISRW:int = 0
        self.IMULT:int = 0
        
        self.PRESSF: float = 0.
        self.PRESSD: float = 0.
        self.TAUF: float = 0.
        self.TAUD: float = 0.
        
        self.SIGXF: float = 0.
        self.SIGYF: float = 0.
        self.SIGXD: float = 0.
        self.SIGYD: float = 0.
        #
        ################################
        #
        self.PRESSF1: float = 0.
        self.PRESSD1: float = 0.
        self.TAUF1: float = 0.
        self.TAUD1: float = 0.
        
        self.SIGXF1: float = 0.
        self.SIGYF1: float = 0.
        self.SIGXD1: float = 0.
        self.SIGYD1: float = 0.
        
        self.PRESSF2: float = 0.
        self.PRESSD2: float = 0.
        self.TAUF2: float = 0.
        self.TAUD2: float = 0.
        
        self.SIGXF2: float = 0.
        self.SIGYF2: float = 0.
        self.SIGXD2: float = 0.
        self.SIGYD2: float = 0.
        
    def GUI(self):
        from LICHA_law_input import Ui_DialogLicha

        if not QtWidgets.QApplication.instance():
            #app = QtWidgets.QApplication(sys.argv)
            QtWidgets.QApplication(sys.argv)
        else:
            #app = QtWidgets.QApplication.instance()
            QtWidgets.QApplication.instance()

        self.DialogLicha = QtWidgets.QDialog()
        mainWin = Ui_DialogLicha()
        mainWin.setupUi(self.DialogLicha)
        
        mainWin.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).clicked.connect(self.DialogLicha.accept)
        mainWin.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).clicked.connect(self.DialogLicha.reject)

        self.DialogLicha.show()
        rsp = self.DialogLicha.exec_()
        
        print('rsp:',rsp)
        if rsp == QtWidgets.QDialog.Accepted:
        
            self.COMMNT = mainWin.lineEditCOMMNT.text()    
        
            self.PRESSF = float(mainWin.lineEditPRESSF1.text())
            self.PRESSD = float(mainWin.lineEditPRESSD1.text())
            self.TAUF = float(mainWin.lineEditTAUF1.text())
            self.TAUD = float(mainWin.lineEditTAUD1.text())

            self.SIGXF = float(mainWin.lineEditPRESSF1.text())
            self.SIGXD = float(mainWin.lineEditPRESSD1.text())
            self.SIGYF = float(mainWin.lineEditTAUF1.text())
            self.SIGYD = float(mainWin.lineEditTAUD1.text())

      
    def WriteLag(self,fileID = None):
        if fileID == None:
            print('# data_file.LAG\n:\n:\n:')
            print('%5i%5i%60s'%(self.IL,self.ITYPE,self.COMMNT))
            print('%5i%5i'%(self.ISOL,self.NINTV))
            print('%10g%10g%10g\n:\n:\n:'%(self.E,self.ANU,self.RHO))
        else:
            fileID.write('%5i%5i%60s\n'%(self.IL,95,self.COMMNT))

            fileID.write('%5i'%(self.IVAL))
            fileID.write('%5i'%(self.IMLIC))
            fileID.write('%5i'%(self.ISRW))
            fileID.write('%5i\n'%(self.IMULT))
                
            if self.IVAL == 1: 
                fileID.write('%10g'%(self.PRESSF))
                fileID.write('%10g'%(self.TAUF))
                fileID.write('%10g'%(self.PRESSD))
                fileID.write('%10g\n'%(self.TAUD))
            if self.IVAL == 5:
                fileID.write('%10g'%(self.SIGXF))
                fileID.write('%10g'%(self.SIGYF))
                fileID.write('%10g'%(self.SIGXD))
                fileID.write('%10g\n'%(self.SIGYD))
    
    def readLag(self,fid):
        l = fid.readline().rstrip()
        if len(l)>0:
            l = l + ' '*20
            self.IVAL = int(l[:5] or '0')
            self.IMLIC = int(l[5:10] or '0')
            self.ISRW = int(l[10:15] or '0')
            self.IMULT = int(l[15:20] or '0')
            l = fid.readline().rstrip()
            if len(l) >=10:
                l = l + ' '*40
                self.PRESSF = float(l[:10].rstrip() or 0.)
                self.TAUF = float(l[10:20].rstrip() or 0.)
                self.PRESSD = float(l[20:30].rstrip() or 0.)
                self.TAUD = float(l[30:40].rstrip() or 0.)
                
                self.SIGXF = self.PRESSF
                self.SIGYF = self.TAUF
                self.SIGXD = self.PRESSD
                self.SIGYD = self.PRESSD
            else:
                print('data missin in LICHA-law ',self.IL)
                


class BINDS_law():
    """ class for elastic constitutive law """
    def __init__(self,lawnumber=0,nalpha = 6):
        self.name: str = 'BINDS'
        self.ISOL: int = 0
        self.NALPHA: int = 6
        self.COMMNT: str = ''
        self.IL: int = lawnumber

        
        
    def GUI(self):
        #from ui_forms.Ui_InputELA import ...

        #if not QtWidgets.QApplication.instance():
        #    QtWidgets.QApplication(sys.argv)
        #else:
        #    QtWidgets.QApplication.instance()

        #DialogEla = QtWidgets.QDialog()
        #mainWin = Ui_DialogEla()
        #mainWin.setupUi(DialogBinds)
        print('NOT IMPLEMENTED')
        pass
        
    def WriteLag(self,fileID = None):
        if fileID == None:
            pass
            
        else:
            
            fileID.write('%5i%5i%60s\n'%(self.IL,30,self.COMMNT))
            fileID.write('%5i%5i%5i%5i%5i\n'%(self.LTYP1,self.NALPHA,self.ICODE,self.NDEF,self.IBEND))
            
            for i in range(self.NALPHA):
                fileID.write('%10g'%(self.ALPHA[i]))
            fileID.write('\n')
            if self.LTYP1 == 1:
                fileID.write('%10g\n'%(self.TRACE)) 
            elif self.LTYP1 == 2:
                fileID.write('%10g%10g%10g\n'%(self.TRACE[0],self.TRACE[1],self.LH1)) 
            elif self.LTYP1 == 3:
                fileID.write('%10g%10g\n'%(self.TRACE[0],self.TRACE[1])) 
                
            
            
    
    def readLag(self,fid):
        self.LTYP1 = int(fid.read(5))
        self.NALPHA = int(fid.read(5))
        self.ICODE = int(fid.read(5))
        self.NDEF = int(fid.read(5))
        self.IBEND = int(fid.read(5))
        next(fid)
            
        self.ALPHA = np.zeros(self.NALPHA)
        for i in range(self.NALPHA):
            self.ALPHA[i] = float(fid.read(10))
        next(fid)
        
        if self.LTYP1 == 1:
            self.TRACE = float(fid.read(10))
            next(fid)
        elif self.LTYP1 == 2:
            self.TRACE = np.zeros(2)
            self.TRACE[0] = float(fid.read(10))
            self.TRACE[1] = float(fid.read(10))
            self.LH1 = float(fid.read(10))
            next(fid)
        elif self.LTYP1 == 2:
            self.TRACE = np.zeros(2)
            self.TRACE[0] = float(fid.read(10))
            self.TRACE[1] = float(fid.read(10))
            next(fid)
            
